package WordGame;
import java.util.ArrayList;
import Main.*;
import GamePlay.*;

import javax.swing.*;
import java.awt.*;


/*
 * Author: Halil Ozercan
 * 
 */
public class AnagramGamePanel extends JPanel{
	
	private ArrayList<String> wordsGuessed;
	private StringBuffer letters;
	private String language;
	private int gameMode;
	
	private GameAnagram anagramGame;
	public JPanel lettersPanel, textboxPanel, guessedWordsPanel, scorePanel;
	public Game hugeGame;
	
	private boolean isWord;
	//Finals
	
	
	public AnagramGamePanel(StringBuffer letters, String language, Game hugeGame){
		
		this.hugeGame = hugeGame;
		this.letters = letters;
		this.language = language;
		anagramGame = new GameAnagram(letters);
		anagramGame.setLanguage(language);
			
		this.setPreferredSize(new Dimension(600,600));
		this.setOpaque(false);
		initPanels();
		//gameLoop();
		
		
	}
	
	public void initPanels(){
		lettersPanel      = new LettersPanel(this);
		textboxPanel      = new TextboxPanel(this);
		guessedWordsPanel = new GuessedWordsPanel(this);
		scorePanel        = new ScorePanel(this);
		add(lettersPanel);
		add(textboxPanel);
		add(guessedWordsPanel);
		add(scorePanel);
	}
	
	/*public void gameLoop(){
		while(anagramGame.score <= 1000){
			if( anagramGame.getComboStep() != 0 ){
				((ScorePanel) scorePanel).updatePanel( anagramGame );
			}
			
			anagramGame.sortLetters(1);
			
		}
	}*/
	
	
	public GameAnagram getAnagramGame(){
		return anagramGame;
	}

	public void update() {

		((ScorePanel) scorePanel).update( this );
		((GuessedWordsPanel) guessedWordsPanel).update( this );
		((LettersPanel) lettersPanel).update( this );
	}

}
