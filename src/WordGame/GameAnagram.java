/*
 * Author: Halil Ozercan
 * 
 */
package WordGame;
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

import Main.CurrentPlayer;
public class GameAnagram {
	public ArrayList<String>  allWords;
	public ArrayList<Integer> guessedWordsID;
	private StringBuffer 	letters = new StringBuffer();
	public int 				score;
	private int 			scoreToAdd;
	private long 			lastKnown;
	private long 			now;
	private int 			comboStep;
	private String 			lang;
	public GameAnagram( String lang ){
		this.score = 0;
		this.lang = lang;
		this.allWords = CurrentPlayer.allWords;
	}
	public GameAnagram( StringBuffer letters){
		this.letters.append(letters);
		this.score = 0;
		this.allWords = CurrentPlayer.allWords;
		
		guessedWordsID = new ArrayList<Integer> ();
	}
	//Will be useless in this project
	public String printLetters(){
		String result;
		result = ""+ letters.charAt(0);
		for (int i = 1; i < this.letters.length(); i++)
			result += ", " + this.letters.charAt(i);
		return result;
	}
	public String getLetters(){
		return letters.toString();
	}
	public String getLanguage(){
		return this.lang;
	}
	public void setLanguage(String lang){
		this.lang = lang;
	}
	/*public void addGuessedWord(String word){
		guessedWords.add(word);
	}*/
	/*public ArrayList<String> getGuessedWords(){
		ArrayList<String> emptyList;
		emptyList = new ArrayList<String> ();
		emptyList.add("No words!");
		if( guessedWords.size()!=0 )
			return guessedWords;
		else
			return emptyList;
			
	}*/
	

	public ArrayList<String> getAllWords( ){
	    return allWords;
	}
	
	public void sortLetters(int dir ){//direction
		if(dir > 0){
		for(int i = 0; i< letters.length(); i++)
			for( int j = i; j< letters.length(); j++)
				if(letters.charAt(i) > letters.charAt(j)){
					char temp = letters.charAt(i);
					letters.setCharAt(i, letters.charAt(j));
					letters.setCharAt(j, temp);
				}
		}
		else if(dir < 0){
			for(int i = 0; i< letters.length(); i++)
				for( int j = i; j< letters.length(); j++)
					if(letters.charAt(i) < letters.charAt(j)){
						char temp = letters.charAt(i);
						letters.setCharAt(i, letters.charAt(j));
						letters.setCharAt(j, temp);
					}
		}
					
	}
	public void addLetter(char a){
		this.letters.append(a);
	}
	public void addLetters(StringBuffer lettersBuffered){
		this.letters.append(lettersBuffered);
	}
	public boolean isWordOkLetters( String word ){
		int[] mappedWord = new int[LanguageMethods.getLengthOfCharmap( lang )];
		int[] mappedLetters = new int[LanguageMethods.getLengthOfCharmap( lang )];
		char[] charmap = new char[LanguageMethods.getLengthOfCharmap( lang )];
		charmap = LanguageMethods.defineCharmap( lang );
 		for ( int i = 0; i < word.length(); i++){
			for( int j = 0 ; j < charmap.length; j++){
				if( word.charAt(i) == charmap[j] )
					mappedWord[j]++;
			}
		}
		for ( int i = 0; i < this.letters.length(); i++){
			for( int j = 0 ; j < charmap.length; j++){
				if( this.letters.charAt(i) == charmap[j] )
					mappedLetters[j]++;
			}
		}
		for( int i = 0 ; i < charmap.length; i++){
			if( mappedWord[i] !=0 && mappedLetters[i]==0 )
				return false;
		}	
		return true;
	}
	
	public boolean isWordOkVocab( String word ){
		return allWords.contains(word);
	}
	
	public int getIDOfWord(String word){
		
		return allWords.indexOf(word);
		
	}
	
	public String getWordOfID(int ID){
		return allWords.get(ID);
	}
	public boolean isTyped(String word){
		for(int i = 0 ; i < guessedWordsID.size() ; i++ ){
			if(guessedWordsID.get(i) == getIDOfWord(word)){
				return true;
			}
		}
		return false;
	}
	public void scoreRefresh( String word){
		switch(word.length())
		{
	        case 3:
	            scoreToAdd=5;
	            break;
	        case 4:
	            scoreToAdd=8;
	            break;
	        case 5:
	            scoreToAdd=14;
	            break;
	        case 6:
	            scoreToAdd=23;
	            break;
	        case 7:
	            scoreToAdd=35;
	            break;
	        case 8:
	            scoreToAdd=50;
	            break;
	        case 9:
	            scoreToAdd=68;
	            break;
	        case 10:
	            scoreToAdd=100;
	            break;
		}
		now = System.nanoTime();
		double seconds = (double)(now-lastKnown) / 1000000000.0;
		if( Math.abs(seconds) < 6 ) {
			switch(comboStep){
				case 1:
					scoreToAdd *= 2;
					break;
				case 2:
					scoreToAdd *= 3;
					break;
				case 3:
					scoreToAdd *= 4;
					break;
				case 4:
					scoreToAdd *= 6;
					break;
				case 5:
					scoreToAdd *= 8;
					break;
			}
			if ( comboStep != 5 )
				comboStep++;
		}
		else
			comboStep = 0;
		score += scoreToAdd;
		lastKnown = now;
		
		
	}
	public int getComboStep(){
		return comboStep;
	}
	public String comboStepToString(){
		switch(comboStep){
		case 1:
			return "2x";
		case 2:
			return "3x";
		case 3:
			return "4x";
		case 4:
			return "6x";
		case 5:
			return "8x";
		}
		return "";
	}
	public void setLetters(StringBuffer newLetters){
		
		this.letters = newLetters;
	}
	
	public ArrayList<String> getAvailableWords( ){
	    int i = 0;
	    ArrayList<String> availableWords = new ArrayList<String>();
	    System.out.println("availableWordsclaalled");

	    while(i < allWords.size()){
	    	if( this.isWordOkLetters( allWords.get(i).toUpperCase() ) ){
	    		availableWords.add(allWords.get(i));
	    	}
	    	i++;
	    
	    }
	    System.out.println(availableWords);
	    return availableWords;
		 
	}
	
}
