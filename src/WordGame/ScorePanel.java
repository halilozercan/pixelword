/*
 * Author: Kaan Aky�z
 * 
 */
package WordGame;

import java.awt.*;

import javax.swing.*;
import javax.swing.GroupLayout.Alignment;

import Main.CurrentPlayer;

public class ScorePanel extends JPanel implements Updatable{
	
	private GameAnagram anagramGame;
	private AnagramGamePanel anagramGamePanel;
	private JLabel comboLabel, scoreLabel;
	
	public ScorePanel(AnagramGamePanel anagramGamePanel){
		
		this.anagramGamePanel = anagramGamePanel;
		setPreferredSize(new Dimension(480,100));
		setOpaque(false);
		this.anagramGame = anagramGamePanel.getAnagramGame();
		comboLabel = new JLabel("");
		scoreLabel = new JLabel("");
		
		if( anagramGame.getComboStep()==0 )
			comboLabel.setText("No Combos");
		else
			comboLabel.setText("Combo = " + anagramGame.comboStepToString());
		
		scoreLabel.setText("Score: " + anagramGame.score);
		
		scoreLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
		scoreLabel.setFont(CurrentPlayer.defFont.deriveFont(36f));
		scoreLabel.setOpaque(false);
		comboLabel.setAlignmentX(Component.RIGHT_ALIGNMENT);
		comboLabel.setFont(CurrentPlayer.defFont.deriveFont(36f));
		comboLabel.setOpaque(false);
		
		add(scoreLabel);
		add(comboLabel);
	}
	public void update( AnagramGamePanel anagramGamePanel ){
		
		GameAnagram anagramGame = anagramGamePanel.getAnagramGame();
		
		if( anagramGame.getComboStep()==0 )
			comboLabel.setText("No Combos");
		else
			comboLabel.setText("Combo = " + anagramGame.comboStepToString());
		
		scoreLabel.setText("Score: " + anagramGame.score);
	}
}
