package WordGame;


import javax.swing.JFrame;
public class Tester3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		JFrame frame = new JFrame("Anagram Play!");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		StringBuffer letters = new StringBuffer();
		
		letters = LanguageMethods.getRandomLetters( 10 , "turkish" ,3,3);
		System.out.println(letters);
		
		AnagramGamePanel newGame = new AnagramGamePanel(letters, "turkish", null);
		
		frame.getContentPane().add(newGame);
		frame.pack();
		frame.setVisible(true);
		
	}

}
