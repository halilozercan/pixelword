/*
 * Author: Halil Ozercan
 * 
 */
package WordGame;

import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;

import Main.CurrentPlayer;

public class LettersPanel extends JPanel implements Updatable{
	
	private GameAnagram anagramGame; 
	private AnagramGamePanel anagramGamePanel;
	private ArrayList<JPanel> panels;
	
	public LettersPanel(AnagramGamePanel anagramGamePanel){
		this.anagramGamePanel = anagramGamePanel;
		this.anagramGame = anagramGamePanel.getAnagramGame();
		panels = new ArrayList<JPanel>();
		setOpaque(false);
		
		
		setPreferredSize(new Dimension(480,200));
		
		for(int i = 0 ; i < anagramGame.getLetters().length() ; i++ ){
			
			JPanel letter = new subPanel( anagramGame.getLetters().charAt(i) );
			panels.add(letter);
			this.add(panels.get(i));
			
		}
	}
	
	private class subPanel extends JPanel{
		JLabel labelLetter;
		public subPanel(char c){
			setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED) );
			setPreferredSize(new Dimension(50,50));
			labelLetter = new JLabel();
			labelLetter.setText(""+c);
			labelLetter.setFont(CurrentPlayer.defFont.deriveFont(30f));
			add(labelLetter);
		}
		
	}
	
	
	public void update(AnagramGamePanel anagramGamePanel){
		
		GameAnagram anagramGame = anagramGamePanel.getAnagramGame();
		
		removeAll();
		
		for(int i = 0 ; i < anagramGame.getLetters().length() ; i++ ){
			
			JPanel letter = new subPanel( anagramGame.getLetters().charAt(i) );
			panels.add(letter);
			this.add(panels.get(i));
			
		}
		
		String word = ((TextboxPanel) anagramGamePanel.textboxPanel).getWord();
		for(int i = 0 ; i < anagramGame.getLetters().length() ; i++ ){
			panels.get(i).setBackground(Color.black);
			for(int j = 0 ; j < word.length(); j++ ){
				if(word.charAt(j) == anagramGame.getLetters().charAt(i)){
					panels.get(i).setBackground(new Color(174,0,14));
				}
			}
		}
	}

}
