/*
 * Author: Halil Ozercan
 * 
 */
package WordGame;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class TextboxPanel extends JPanel {
	
	private String typedWord, currentWord;
	private GameAnagram anagramGame;
	private AnagramGamePanel anagramGamePanel;
	private JTextField textbox;
	private JButton button, finish;
	
	public TextboxPanel(AnagramGamePanel anagramGamePanel){
		
		this.anagramGamePanel = anagramGamePanel;
		this.anagramGame = anagramGamePanel.getAnagramGame();
		
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		setPreferredSize(new Dimension(480,100));
		setOpaque(false);
		
		textbox = new JTextField();
		button = new JButton("Try!");
		finish = new JButton("Go to Last 5 Secs");
		
		textbox.setPreferredSize(new Dimension(200,40));
		textbox.setMaximumSize(new Dimension(200,40));
		//textbox.setFont(new Font("Comic Sans Ms", Font.CENTER_BASELINE, 15));
		textbox.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		button.setAlignmentX(Component.CENTER_ALIGNMENT);
		finish.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		textbox.addKeyListener(new TextboxListener() );
		button.addActionListener(new ButtonActionListener() );
		finish.addActionListener(new ButtonActionListener() );
		
		add(textbox);
		add( Box.createRigidArea( new Dimension( 0, 10)));
		add(button);
		add(finish);
	}
	
	public String getWord(){
		return textbox.getText().toUpperCase();
	}
	private class TextboxListener implements KeyListener{
		public void keyPressed(KeyEvent e){
			if(e.getKeyChar() == KeyEvent.VK_ENTER){
				button.doClick();
			}
		}
		public void keyTyped(KeyEvent e){
			
		}
		public void keyReleased(KeyEvent e){
			
			String word = textbox.getText().toUpperCase();
			if (e.getKeyChar() != KeyEvent.VK_ENTER)
			{
				if( !anagramGame.isTyped( textbox.getText() ) ){
					
					if( !anagramGame.isWordOkLetters( word ) ){
						
						textbox.setBackground(new Color(174,0,14));
						button.setEnabled(false);
					}
					else{
						button.setEnabled(true);
						textbox.setBackground(new Color(80,80,80));
					}
				}
				else{
					textbox.setBackground(Color.yellow);
					button.setEnabled(false);
				}
				anagramGamePanel.update();
			}
		}
	}
	private class ButtonActionListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			if(e.getSource()==button){
				String word = textbox.getText();
				if( button.isEnabled()){
					if( !anagramGame.isWordOkVocab( word ) ){
						
						textbox.setBackground(Color.red);
					}
					else{
						if(anagramGame.isWordOkLetters( word ) && !anagramGame.isTyped( textbox.getText() ))
						{
							anagramGame.scoreRefresh( word );
							textbox.setBackground(Color.green);
							anagramGame.guessedWordsID.add( anagramGame.getIDOfWord( textbox.getText() ) );
							anagramGamePanel.update();
						}
					}
					
					textbox.setText("");
					
				}
			}
			else if(e.getSource()==finish){
				anagramGamePanel.hugeGame.timeCount = 175000;
			}
			
		}
	}
}
