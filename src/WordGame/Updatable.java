/*
 * Author: Halil Ozercan
 * 
 */
package WordGame;

public interface Updatable {
	public void update(AnagramGamePanel anagramGamePanel);

}
