/*
 * Author: Halil Ozercan
 * 
 */
package WordGame;

public class Charmaps {
	public static char[] english      = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
	public static int[]  englishVowel = { 0, 4, 8, 14, 20};
	public static char[] turkish      = { 'A', 'B', 'C', '�', 'D', 'E', 'F', 'G', '�', 'H', 'I', '�', 'J', 'K', 'L', 'M', 'N', 'O', '�', 'P', 'R', 'S', '�', 'T', 'U', '�' ,'V', 'Y', 'Z'};
	public static int[]  turkishVowel = { 0, 5, 10, 11, 17, 18, 24, 25};
	public static char[] spanish      = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', '�', 'J', 'K', 'L', 'M', 'N', '�', 'O', '�', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
	public static int[]  spanishVowel = { 0, 4, 8, 9, 16, 17, 23};
	public static char[] german       = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', '�', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
	public static int[]  germanVowel  = { 0, 4, 8, 15, 21};
	public Charmaps(){
		
	}
}
