/*
 * Author: Halil Ozercan
 * 
 */
package WordGame;

import java.awt.*;
import java.util.Random;

import javax.swing.*;

public class GuessedWordsPanel extends JPanel {
	
	private GameAnagram anagramGame;
	private AnagramGamePanel anagramGamePanel;
	private JLabel nextWord;
	public GuessedWordsPanel(AnagramGamePanel anagramGamePanel){
		
		this.anagramGame = anagramGamePanel.getAnagramGame();
		this.anagramGamePanel = anagramGamePanel;
		
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		setPreferredSize(new Dimension(480,200));
		setOpaque(false);
		this.anagramGame = anagramGame;
		for(int i = 0 ; i < anagramGame.guessedWordsID.size() ; i++ ){
			nextWord = new JLabel( anagramGame.allWords.get( anagramGame.guessedWordsID.get(i) ) );
			add( nextWord );
		}
	}
	public void update(AnagramGamePanel anagramGamePanel){
		
		GameAnagram anagramGame = anagramGamePanel.getAnagramGame();
		this.removeAll();
		for(int i = 0 ; i < anagramGame.guessedWordsID.size() ; i++ ){
			nextWord = new JLabel( anagramGame.allWords.get( anagramGame.guessedWordsID.get(i) ) );
			nextWord.setFont(new Font("Calibri", Font.CENTER_BASELINE, 20));
			nextWord.setForeground(getRandomColor() );
			add( nextWord );
			add( Box.createRigidArea(new Dimension(10,0)));
		}
	}
	public static Color getRandomColor(){
		Random gen = new Random();
		int r = gen.nextInt(3) * 127;
		int g = gen.nextInt(3) * 127;
		int b = gen.nextInt(3) * 127;
		return new Color( r, g ,b ) ;
	}
}
