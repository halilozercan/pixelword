/*
 * Author: Halil Ozercan
 * 
 */
package WordGame;
import java.awt.List;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;
public class LanguageMethods {
	public static int getLengthOfCharmap( String lang ){
		
		if(lang.equals("english"))
			return Charmaps.english.length;
		
		else if(lang.equals("turkish"))
			return Charmaps.turkish.length;
		
		else if(lang.equals("spanish"))
			return Charmaps.spanish.length;
		
		return 0;
	}
	public static char[] defineCharmap( String lang ){
		char[] charmap = new char[getLengthOfCharmap( lang )];
		
		if(lang.equals("english"))
			System.arraycopy( Charmaps.english, 0, charmap, 0, charmap.length );
		
		else if(lang.equals("turkish"))
			System.arraycopy( Charmaps.turkish, 0, charmap, 0, charmap.length );
		
		else if(lang.equals("spanish"))
			System.arraycopy( Charmaps.spanish, 0, charmap, 0, charmap.length );
		
		return charmap;
	}
	public static int getLengthOfCharmapVowel( String lang ){
		
		if(lang.equals("english"))
			return Charmaps.englishVowel.length;
		
		else if(lang.equals("turkish"))
			return Charmaps.turkishVowel.length;
		
		else if(lang.equals("spanish"))
			return Charmaps.spanishVowel.length;
		
		
		return 0;
	}
	public static int[] defineCharmapVowel( String lang ){
		int[] charmap = new int[getLengthOfCharmapVowel( lang )];
		
		if(lang.equals("english"))
			System.arraycopy( Charmaps.englishVowel, 0, charmap, 0, charmap.length );
		
		else if(lang.equals("turkish"))
			System.arraycopy( Charmaps.turkishVowel, 0, charmap, 0, charmap.length );
		
		else if(lang.equals("spanish"))
			System.arraycopy( Charmaps.spanishVowel, 0, charmap, 0, charmap.length );
		
		return charmap;
	}
	public static boolean isVowel(int position, int[] vowels){
		for(int i = 0 ; i < vowels.length ; i++){
			if(position == vowels[i])
				return true;
		}
		
		return false;
		
	}
	public static StringBuffer getRandomLetters( int capacity , String lang, int minConsonant, int minVowels){
		Random gen = new Random();
		
		char[] charmap = new char[getLengthOfCharmap( lang )];
		charmap = defineCharmap( lang );
		int[]  charmapVowel = new int[getLengthOfCharmapVowel( lang )];
		charmapVowel = defineCharmapVowel( lang );
		
		StringBuffer sendingLetters = new StringBuffer();
		int i = 0;
		while(i < minVowels){
			
			int position = gen.nextInt(charmap.length);
			
			if( sendingLetters.indexOf ( Character.toString ( charmap[position] ) ) == -1 && isVowel(position, charmapVowel)){
				sendingLetters.append( charmap[position] );
				i++;
			}
		}
		
		i = 0;
		
		while(i < minConsonant){
			int position = gen.nextInt(charmap.length);
			
			if( sendingLetters.indexOf ( Character.toString ( charmap[position] ) ) == -1 && !isVowel(position, charmapVowel)){
				sendingLetters.append( charmap[position] );
				i++;
			}
			
		}
		
		while( sendingLetters.length() != capacity ){
			int position = gen.nextInt(charmap.length);
			
			if( sendingLetters.indexOf ( Character.toString ( charmap[position] ) ) == -1 )
				sendingLetters.append( charmap[position] );
		
		}
		
		//Shuffle starts
        ArrayList<Character> characters = new ArrayList<Character>();  
        for(char c : sendingLetters.toString().toCharArray()) {  
            characters.add(c);  
        }  
        Collections.shuffle(characters);  
        StringBuffer sb = new StringBuffer();  
        for(char c : characters) {  
            sb.append(c);  
        }  
      
		
		return sb;
	}
	public static StringBuffer getRandomWord( String lang ){
	    String PATH = lang + ".voc";

	    StringBuffer randomWord = new StringBuffer();
	    
	    int numberOfWords;
	    int randomIndex;

	    FileReader reader = null;
		try {
			reader = new FileReader(PATH);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	    Scanner scanFile  = new Scanner(reader);

	    for( numberOfWords = 0; scanFile.hasNext(); scanFile.nextLine(), numberOfWords++);
	    
	    try {
			reader.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	    scanFile.close();

	    try {
			reader   = new FileReader(PATH);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	    scanFile = new Scanner(reader);

	    randomIndex = (int) (Math.random() * numberOfWords);

	    for( int i = 0; i < randomIndex; i++)
	    	scanFile.nextLine();

	    randomWord = new StringBuffer(scanFile.nextLine());

	    try {
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	    scanFile.close();

	    return randomWord;
	}
	
	public static int getIDOfWord(String word, String lang){
		return getAllWords(lang).indexOf(word);
		
	}
	
	public static String getWordOfID(int ID, String lang){
		return getAllWords(lang).get(ID);
	
	}
	
	public static ArrayList<String> getAllWords(String lang ){
	    String PATH = lang + ".voc";
	    ArrayList<String> result = new ArrayList<>();

	    FileReader reader = null;
		try {
			reader = new FileReader(PATH);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	    Scanner scanFile  = new Scanner(reader);

	    while(scanFile.hasNext()){
	    	String tempWord = scanFile.next();
	    	result.add(tempWord);
	    		
	    }
	    
	    try {
			reader.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	    scanFile.close();
	    return result;
	}
	
}
