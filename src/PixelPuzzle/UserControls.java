/*
 * Author: Halil Ozercan
 * 
 */
package PixelPuzzle;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Drawable;


public class UserControls {
	private Animations anims;
	private GameModel model;
	private Calculations calcs;
	public UserControls(Animations anims, GameModel model, Calculations calcs){
		this.anims = anims;
		this.model = model;
		this.calcs = calcs;
	}
	
	public void keyboard(){
		if (Keyboard.isKeyDown( Keyboard.KEY_UP))
			anims.moveCam(0,-5f);
		if (Keyboard.isKeyDown( Keyboard.KEY_DOWN))
			anims.moveCam(0,5f);
		if (Keyboard.isKeyDown( Keyboard.KEY_RIGHT))
			anims.moveCam(5f,0);
		if (Keyboard.isKeyDown( Keyboard.KEY_LEFT))
			anims.moveCam(-5f,0);
		
	}
	
	public void zoom(){
		if(Mouse.isButtonDown(2)){
			float dZoom = Mouse.getDY();
			if (model.radius + dZoom >= 10 && model.radius + dZoom <= 1000 ){
				model.radius += dZoom;
				anims.moveCam(0,0);
			}
		}
	}
	public float mouse1(){
		if(Mouse.isButtonDown(0))
		{
			float dx = Mouse.getDX();
			float dy = -Mouse.getDY();
			
			anims.moveCam( dx, dy );
			
			calcs.puzzleControl(model.cam);
			return (float) Math.sqrt( dx*dx + dy*dy);
		}	
		return 0;
	}
}
