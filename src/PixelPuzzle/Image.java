/*
 * Author: Halil Ozercan
 * 
 */
package PixelPuzzle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Random;

import javax.imageio.ImageIO;

import org.lwjgl.BufferUtils;
import org.lwjgl.util.glu.GLU;

import static org.lwjgl.opengl.GL11.*;



public class Image {
	public int w,h;
	public boolean a[][];
	public int r[][];
	public int g[][];
	public int b[][];
	
	public Image(String fileName)
	{

		BufferedImage img = read(fileName);
		w = img.getWidth();
		h = img.getHeight();

		a=new boolean[w][h];
		r=new int[w][h];
		g=new int[w][h];
		b=new int[w][h];

		for(int i=0;i<w ; i++)
			for(int j=0;j<h ; j++)
			{
				// The old system of color getting
				/*int rgb = img.getRGB(i,j);
				int alpha = (short) (rgb >> 24) & 0xff;
				 a[i][j] = true;
				r[i][j]= (short) (( rgb & (0x00ff0000)) >> 16);
				g[i][j]= (short) (( rgb & (0x0000ff00)) >> 8);
				b[i][j]= (short) (( rgb & (0x000000ff)) >> 0);*/
				
				int rgb = img.getRGB(i,j);
				int alpha = (short) (( rgb & (0x00ff0000)) >> 16);
				if( alpha < 200 ){

					Random gen = new Random();
					a[i][j] = true;
					r[i][j]=  (gen.nextInt(3) * 64) + 128;
					g[i][j]=  (gen.nextInt(3) * 64) + 128;
					b[i][j]=  (gen.nextInt(3) * 64) + 128;
				}
			}
	}
	

	private static BufferedImage read(String fileName){

		BufferedImage imj=null;
		try {
			imj = ImageIO.read(new File(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return imj;
	}
	
	public static void glLoadImage(String fileName, int tex){

		BufferedImage img = read(fileName);
		int w = img.getWidth();
		int h = img.getHeight();
		
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, tex);
		ByteBuffer buffer = BufferUtils.createByteBuffer(w*h*3);
		
		for(int y = 0; y < h; y++){
			for(int x = 0; x < w; x++){
				int pixel = img.getRGB(x, y);
				buffer.put((byte) ((pixel >> 16) & 0xFF));     // Red component
				buffer.put((byte) ((pixel >> 8) & 0xFF));      // Green component
				buffer.put((byte) (pixel & 0xFF));               // Blue component
			}
		}
        buffer.rewind();
		GLU.gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB, w, h, GL_RGB , GL_UNSIGNED_BYTE, buffer);
	}
}
