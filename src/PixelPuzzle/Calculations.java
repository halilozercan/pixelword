/*
 * Author: Halil Ozercan
 * 
 */
package PixelPuzzle;
import java.nio.FloatBuffer;

import org.lwjgl.input.Mouse;

public class Calculations {
	private GameModel model;
	public Calculations(GameModel model){
		this.model = model;
	}
	
	public void floatToBuffer(FloatBuffer m, float[] matrix, int size){
		for(int i=0 ; i < size ; i++){
			m.put(matrix[i]);
		}
		m.rewind();
	}
	
	public Image takeLetterImage(char c){
		Image resim;
		resim = new Image("Alphabet/" + c + ".png");
		return resim;
	}
	public boolean puzzleControl( float[] cam ){
		if( ( distanceToRightAngle(cam) < model.solutionSens ) && !Mouse.isButtonDown(0))
			return true;
		else
			return false;
	}
	public float distanceToRightAngle(float[] cam){
		float diff1[]=new float[3];
		float a[]={0,0,-model.radius};
		SUM(diff1,a,cam,-1);
		
		float diff2[]=new float[3];
		float a2[]={0,0,model.radius};
		SUM(diff2,a2,cam,-1);
		
		//return Math.min(LEN(diff1), LEN(diff2));
		return LEN(diff1);
	}
	public void CROSS(float[] v, float[] p, float[] q) {
		v[0]= p[2]*q[1] - p[1]*q[2];
		v[1]=-p[2]*q[0] + p[0]*q[2];
		v[2]= p[1]*q[0] - p[0]*q[1];
	}

	public void MULT(float[] v, float f) {
		v[0]*=f;
		v[1]*=f;
		v[2]*=f;
	}

	public float LEN(float[] v) { // length of vector
		return (float)Math.sqrt(v[0]*v[0]+v[1]*v[1]+v[2]*v[2]);
	}

	public void SUM(float[] v1, float[] v2,float v3[], float f) {// v1=v2+v3*f
		v1[0]=v2[0]+v3[0]*f;
		v1[1]=v2[1]+v3[1]*f;
		v1[2]=v2[2]+v3[2]*f;
	}
}
