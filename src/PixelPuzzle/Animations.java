/*
 * Author: Halil Ozercan
 * 
 */
package PixelPuzzle;
import static org.lwjgl.opengl.GL11.*;

import java.util.Random;

import org.lwjgl.opengl.Display;
import org.lwjgl.util.glu.GLU;
import GamePlay.*;

public class Animations {
	//Coordinates to implement cubes and its physical conditions
	public static float[][] cube={
		{1,1,1},
		{-1,1,1},
		{-1,-1,1},
		{1,-1,1},
		{1,1,-1},
		{-1,1,-1},
		{-1,-1,-1},
		{1,-1,-1}};
	public static float[][] cubeNormals={
		{0,0,1},
		{0,1,0},
		{0,0,-1},
		{-1,0,0},
		{0,-1,0},
		{1,0,0}};
	public static int [][] cubeIndices={
		{0,1,2,3},
		{0,1,5,4},
		{4,5,6,7},
		{1,2,6,5},
		{2,3,7,6},
		{3,0,4,7}
	};
	//Cube Matrix
	float cubeMat[]=new float[16];
	
	private GameModel model;
	private Calculations calcs;
	
	public Animations(GameModel model){
		this.model = model;
		calcs = new Calculations(model);
	}
	
	public void gameStart(){
		model.cam[0] = (float)Math.PI * (model.radius);
		for(int step = 0 ; step < (model.radius/2) * Math.PI ; step ++){
			glClearColor(0.5f,0.5f,0.8f,0);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			glFrustum( -model.ratio, model.ratio,-1,1, 5, 50000); 

			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();
			GLU.gluLookAt(model.cam[0], model.cam[1], model.cam[2], 0, 0, 0, model.camUp[0], model.camUp[1], model.camUp[2]);
			glTranslatef(-model.resim.w, -model.resim.h, 0);
			glEnable(GL_DEPTH_TEST);

			
			for(int i=0;i<model.resim.w;i++) 
				for(int j=0;j<model.resim.h;j++)
				{
					if(model.resim.a[i][j]) 
						drawCube(i, j, 0, model.resim.r[i][j],model.resim.g[i][j],model.resim.b[i][j], 1,1);
					
				}
			moveCam(-1,0);
			Display.update();
		}
	}
	public void moveCam(float difX, float difY){
		calcs.SUM(model.cam,model.cam,model.camUp,difY/model.mouseSens);
		calcs.MULT(model.cam,model.radius/calcs.LEN(model.cam));
		calcs.CROSS(model.camUp,model.cam,model.camRight);
		calcs.MULT(model.camUp,1/calcs.LEN(model.camUp));
		
		calcs.SUM(model.cam,model.cam,model.camRight,difX/model.mouseSens);
		calcs.MULT(model.cam,model.radius/calcs.LEN(model.cam));
		calcs.CROSS(model.camRight,model.camUp,model.cam);
		calcs.MULT(model.camRight,1/calcs.LEN(model.camRight));
	}
	public void translateAnimation(){
		if(model.isPuzzled == true)
			model.finalAnimation--;
			
		if (model.finalAnimation == 0){
			
			for(int step = 0 ; step < (model.radius/2) * Math.PI ; step ++){
				moveCam(1,0);
				Display.update();
				model.renderGL();
			}
			model.setLetterTurn( model.getLetterTurn()+1 );

			model.hugeGame.update();
			if( model.getLetterTurn() < model.getWord().length() )
				model.resim = calcs.takeLetterImage( model.getWord().charAt(model.getLetterTurn() ) );
			
			
			/*for(int step = 0 ; step < (model.radius/2) * Math.PI ; step ++){
				Animations.moveCam(1,0);
				Display.update();
				model.renderGL();
			}
*/
			model.isPuzzled = false;
			randomLookAt();
			model.finalAnimation = 100;
		}
	}
	
	public void randomLookAt(){
		float randomX = 0, randomY = 0;
		Random rd = new Random(); 
		
		for(int i = 0 ; i < 100 ; i++ ){
			if( i % 50 == 0 ){
				randomX = (rd.nextInt(5)-2)*5;
				randomY = (rd.nextInt(5)-2)*5;
			}
			moveCam(randomX, randomY);
			Display.update();
			model.renderGL();
		}
		if( calcs.puzzleControl(model.cam) ){
			randomLookAt();
		}
		
	}
	
	
	private void weightSumMatrix16(float[] a,float[] b,float r){
		for(int i=0;i<12;i++)
			a[i]+=b[i]*r;
		for(int i=0;i<12;i+=4)
		{
			double l = Math.sqrt( a[i]*a[i]+ a[i+1]*a[i+1] + a[i+2]*a[i+2]);
			a[i]/=l;
			a[i+1]/=l;
			a[i+2]/=l;
		}
	}
	
	public void drawCube(float x,float y,float z , int r, int g, int b, float size,float dt)
	{
		glPushMatrix();
		
		glColor3f(r/255f,g/255f,b/255f);
			
		calcs.CROSS(model.dir, model.camUp, model.camRight);
		calcs.MULT(model.dir,1.f/calcs.LEN(model.dir));

		float matrix[]=
			{model.camRight[0], model.camRight[1], model.camRight[2], 0.0f,     
				model.camUp[0], model.camUp[1], model.camUp[2], 0.0f,                        
				model.dir[0], model.dir[1], model.dir[2], 0.0f,  
				2*x, 2*y, 2*z, 1.0f};
		
		weightSumMatrix16(cubeMat, matrix, dt/4);
		cubeMat[12]=matrix[12];
		cubeMat[13]=matrix[13];
		cubeMat[14]=matrix[14];
		cubeMat[15]=matrix[15];
		calcs.floatToBuffer(model.m, cubeMat, 16);
		if(!model.isPuzzled)
			glMultMatrix(model.m);
		if(model.isPuzzled)
			glTranslatef(2*x, 2*y, 2*z);
		
		glBegin(GL_QUADS);
		for(int i=0;i<6;i++){
			glNormal3f(cubeNormals[i][0],cubeNormals[i][1],cubeNormals[i][2]);
			glTexCoord2f(0, 0);
			glVertex3f(cube[cubeIndices[i][0]][0] * size,cube[cubeIndices[i][0]][1] * size,cube[cubeIndices[i][0]][2] * size);
			glTexCoord2f(0, 1);
			glVertex3f(cube[cubeIndices[i][1]][0] * size,cube[cubeIndices[i][1]][1] * size,cube[cubeIndices[i][1]][2] * size);
			glTexCoord2f(1, 1);
			glVertex3f(cube[cubeIndices[i][2]][0] * size,cube[cubeIndices[i][2]][1] * size,cube[cubeIndices[i][2]][2] * size);
			glTexCoord2f(1, 0);
			glVertex3f(cube[cubeIndices[i][3]][0] * size,cube[cubeIndices[i][3]][1] * size,cube[cubeIndices[i][3]][2] * size);
			}
		glEnd();

		
	
		glColor3f( 0f, 0f, 0.5f);
		for(int i=0;i<6;i++){
			glBegin(GL_LINE_LOOP);
			glVertex3f(cube[cubeIndices[i][0]][0] * size,cube[cubeIndices[i][0]][1] * size,cube[cubeIndices[i][0]][2] * size);
			glVertex3f(cube[cubeIndices[i][1]][0] * size,cube[cubeIndices[i][1]][1] * size,cube[cubeIndices[i][1]][2] * size);
			glVertex3f(cube[cubeIndices[i][2]][0] * size,cube[cubeIndices[i][2]][1] * size,cube[cubeIndices[i][2]][2] * size);
			glVertex3f(cube[cubeIndices[i][3]][0] * size,cube[cubeIndices[i][3]][1] * size,cube[cubeIndices[i][3]][2] * size);
			glEnd();
		}
			
		glPopMatrix();
	}
}
