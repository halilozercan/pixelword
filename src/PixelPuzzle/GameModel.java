/*
 * Author: Halil Ozercan
 * 
 */
package PixelPuzzle;

import java.nio.FloatBuffer;
import java.util.Random;

import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.*;

import org.lwjgl.util.glu.GLU;

import java.awt.*;
import java.awt.Canvas;



import static org.lwjgl.opengl.GL11.*;
import Main.*;
import GamePlay.*;

public class GameModel {
 
	public Image resim;
 
	long lastFrame;
	float rand;
	float dx , dy;
	public float solutionSens = 8f;
	public float mouseSens = 1;
	
	public boolean isPuzzled = false;
	
	float finalAnimation = 100; 
	float randomX , randomY;

	public float radius=80;
	
	public float cam[]={0,0,radius};
	public float camUp[]={0,-1,0};
	public float camRight[]={1,0,0};
	public float dir[] = {0,0,1};
	

	
	public float initMatrix[]=
			{1, 0, 0, 0.0f,     
				0, -1, 0, 0.0f,                        
				0, 0, radius, 0.0f,  
				0, 0, 0, 1.0f}; 

	public float ratio;

	public FloatBuffer m;
	public FloatBuffer mInit;
 
	private FloatBuffer matSpecular;
	private FloatBuffer lightPosition;
	private FloatBuffer lightPosition2;
	private FloatBuffer whiteLight; 
	private FloatBuffer lModelAmbient;
	
	private String word;
	private Canvas canvas;
	private int letterTurn;
	private Calculations calcs;
	private Animations anims;
	private UserControls ctrls;
	
	private boolean running;
	private boolean everStarted;
	private Thread gameThread;

	private long lastTime;

	private int textureId;
	public Game hugeGame;
	
	public GameModel(String word, Canvas canvas, Game hugeGame){
		m=BufferUtils.createFloatBuffer(16);
		mInit=BufferUtils.createFloatBuffer(16);
		
		initLightArrays();
		
		this.word = word;
		this.canvas = canvas;
		this.letterTurn = 0;
		this.hugeGame = hugeGame;
		
		
		anims = new Animations(this);
		calcs = new Calculations(this);
		ctrls = new UserControls(anims, this, calcs);
		
		
	}
	
	public boolean isRunning(){
		return running;
	}
	
	public String getWord(){
		return this.word;
	}
	
	public boolean everStarted(){
		return everStarted;
	}
	
	public int getLetterTurn(){
		return this.letterTurn;
	}
	
	public void setLetterTurn( int newLetterTurn ){
		this.letterTurn = newLetterTurn;
	}
	
	public StringBuffer getFoundSoFar(){
		return new StringBuffer(word.substring(0,letterTurn));
	}
	
	public void destroyDisplay(){
		this.running = false;
	}
	
	public void start() {
		gameThread = new Thread() {
			public void run() {
				running = true;
				try {
					Display.setParent(canvas);
					Display.setDisplayMode(new org.lwjgl.opengl.DisplayMode(canvas.getWidth(), canvas.getHeight()));
					ratio=(float)canvas.getWidth()/(float)canvas.getHeight();
					Display.create();
				} catch (LWJGLException e) {
					e.printStackTrace();
					System.exit(0);
				}
				gameLoop();
			}
		};
		gameThread.start();
		everStarted = true;
		
	}
	
	public void gameLoop(){
		
		//initGL();
		resim = calcs.takeLetterImage( word.charAt(letterTurn) );
		//initGL();
		anims.randomLookAt();
		initGL();
		
		while (running) {
			update();
			
			renderGL();
			
			Display.update();
			Display.sync(60); // cap fps to 60fps
			anims.translateAnimation();
			
			if( letterTurn >= word.length() ){
				running = false;
				hugeGame.allLettersFound();
			}
				
		}

		Display.destroy();
	}
 
	public void update() {


		if(!isPuzzled){
			ctrls.keyboard();
		
			ctrls.zoom();
			if( calcs.puzzleControl(cam) )
				isPuzzled = true;
			ctrls.mouse1();
		}
		
		
	}
	
	public void initGL() {
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0, 600, 0, 600, 1, -1);
		glMatrixMode(GL_MODELVIEW);

		glMaterial(GL_FRONT, GL_SPECULAR, matSpecular);				// sets specular material color
		glMaterialf(GL_FRONT, GL_SHININESS, 60.0f);					// sets shininess
		
		glLight(GL_LIGHT0, GL_POSITION, lightPosition);				// sets light position
		glLight(GL_LIGHT0, GL_SPECULAR, whiteLight);				// sets specular light to white
		glLight(GL_LIGHT0, GL_DIFFUSE, whiteLight);					// sets diffuse light to white
		
		glLight(GL_LIGHT1, GL_POSITION, lightPosition2);				// sets light position
		glLight(GL_LIGHT1, GL_SPECULAR, whiteLight);				// sets specular light to white
		glLight(GL_LIGHT1, GL_DIFFUSE, whiteLight);					// sets diffuse light to white
		
		glLightModel(GL_LIGHT_MODEL_AMBIENT, lModelAmbient);		// global ambient light 
		
		glEnable(GL_LIGHTING);										// enables lighting
		glEnable(GL_LIGHT0);										// enables light0
		glEnable(GL_LIGHT1);
		
		glEnable(GL_COLOR_MATERIAL);								// enables opengl to use glColor3f to define material color
		glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);			// tell opengl glColor3f effects the ambient and diffuse properties of material
		//----------- END: Variables & method calls added for Lighting Test -----------//
		
		textureId = glGenTextures();
		Image.glLoadImage("texture.png", textureId);
	}
	
	private void stop() {
		running = false;
		try {
			gameThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public int depth(int i, int j){
		int max;
		i -= 8;
		j -= 8;
		max = (int) Math.sqrt(16*16-i*i-j*j);
		return max;
	}
	
	private void initLightArrays() {
		matSpecular = BufferUtils.createFloatBuffer(4);
		matSpecular.put(1.0f).put(1.0f).put(1.0f).put(1.0f).flip();
		
		lightPosition = BufferUtils.createFloatBuffer(4);
		lightPosition.put(1.0f).put(1.0f).put(1.0f).put(1.0f).flip();
		
		lightPosition2 = BufferUtils.createFloatBuffer(4);
		lightPosition2.put(-1.0f).put(-1.0f).put(-1.0f).put(1.0f).flip();
		
		whiteLight = BufferUtils.createFloatBuffer(4);
		whiteLight.put(1.0f).put(1.0f).put(1.0f).put(1.0f).flip();
		
		lModelAmbient = BufferUtils.createFloatBuffer(4);
		lModelAmbient.put(0.5f).put(0.5f).put(0.5f).put(1.0f).flip();
	}

	public void renderGL() {
		long time  = System.currentTimeMillis();
		long dt=time-lastTime;
		lastTime=time;
		
		glShadeModel(GL_SMOOTH);
		glClearColor(6/255f, 12/255f , 24/255f ,0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glFrustum( -ratio, ratio,-1,1, 3, 100000); 
		
		glLight(GL_LIGHT0, GL_POSITION, lightPosition);	
		
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		if(!isPuzzled)
			GLU.gluLookAt(cam[0], cam[1], cam[2], 0, 0, 0, camUp[0], camUp[1], camUp[2]);
		else
			GLU.gluLookAt(cam[0], cam[1], cam[2], 0, 0, 0, (camUp[0]/100)*finalAnimation, camUp[1]-(((camUp[1]+1)/100)*(100-finalAnimation)), 0);
		glTranslatef(-resim.w, -resim.h, 0);
		glEnable(GL_DEPTH_TEST);

		glEnable(GL_LIGHTING);
		Random rd=new Random(1);
		float timething=dt/1000f;
		for(int i=0;i<resim.w;i++) 
			for(int j=0;j<resim.h;j++)
			{
				if(resim.a[(int)i][(int)j]) {
					if(!isPuzzled)
						rand = rd.nextInt(depth(i,j))-8;
					else
						rand = ( ( rd.nextInt(depth(i,j))-8 ) * (finalAnimation/200) );
					anims.drawCube(i, j, rand, resim.r[i][j],resim.g[i][j],resim.b[i][j], 1,timething);
				}
			}

		glDisable(GL_LIGHTING);
		
	}
	
}