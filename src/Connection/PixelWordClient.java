/*
 * Author: Cagla Gagaci
 * PixelWordClient creates the link between server and user. This class includes 2 basic methods that sends and receives data
 */
package Connection;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.JOptionPane;

import Main.Match;
import Main.Player;


public class PixelWordClient {
	private static final String LOGIN_SUCCESS = "LOGIN_SUCCESS";
    private static final String LOGIN_FAIL = "LOGIN_FAIL";
    

    private static int PORT = 9090;
    private Socket socket;
    private static BufferedReader in;
    private static PrintWriter out;
    private static ObjectInputStream ois;
    private static ObjectOutputStream oos;
    
    public static boolean connectStatus;

    /**
     * Constructs the client by connecting to a server, laying out the
     * GUI and registering GUI listeners.
     * @throws IOException 
     */
    public PixelWordClient(String serverAddress) throws IOException {

        // Setup networking
        try {
			socket = new Socket(serverAddress, PORT);

	        in = new BufferedReader(new InputStreamReader(
	            socket.getInputStream()));
	        out = new PrintWriter(socket.getOutputStream(), true);
	        PixelWordClient.connectStatus = true;
	        
	        ois = new ObjectInputStream(socket.getInputStream());
            oos = new ObjectOutputStream(socket.getOutputStream());
            
	        
	        
	        out.println(socket.getLocalAddress());
	        
		} catch (UnknownHostException e) {
			JOptionPane.showMessageDialog(null, "Could not connect to server");
			PixelWordClient.connectStatus = false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, "Could not connect to server");
			PixelWordClient.connectStatus = false;
		}
           
    }
    
    public static void sendData(String data){
    	out.println(data);
    }
    
    public static void sendObject(Object o){
    	try {
			oos.writeObject(o);
			oos.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
    
    /*public static void sendMatch(Match m) throws IOException{
    	oos.writeObject(m);
    }*/
    
    public static String readData(){
    	try {
			return in.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
    }
    
    public static Object readObject(){
    	try {
			try {
				return ois.readObject();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
    }
    
    /*public static Match readMatch() throws ClassNotFoundException{
    	try {
			return (Match)ois.readObject();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
    }*/
   
}
