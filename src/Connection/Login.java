/*
 * Author: Safa Onur Sahin
 * This class tends to create welcome page of project and set nimbus look and feel
 */
package Connection;

import java.awt.*;
import java.io.*;
import java.sql.Time;
import java.util.*;

import javax.swing.*;
import javax.swing.UIManager.LookAndFeelInfo;

import Main.CurrentPlayer;

public class Login extends JFrame{
	public Login(){
		
		this.setTitle("Pixel Word - Developer Preview");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		this.getContentPane().add(new Tabbed(this));
		this.pack();
		this.setVisible(true);
		this.setResizable(false);
		
    	Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
    	 
    	// Determine the new location of the window
    	int w = this.getSize().width;
    	int h = this.getSize().height;
    	int x = (dim.width-w)/2;
    	int y = (dim.height-h)/2;
    	 
    	// Move the window
    	this.setLocation(x, y);
        
	}
	public static void main(String[] args) throws IOException{
		
		FileReader reader = null;
		try {
			reader = new FileReader("server.ini");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	    Scanner scanFile  = new Scanner(reader);
		
		String serverAddress = (args.length == 0) ? scanFile.next() : args[1];
		PixelWordClient client = new PixelWordClient(serverAddress);
		
		
        try {
			UIManager.setLookAndFeel("com.jtattoo.plaf.hifi.HiFiLookAndFeel");

            UIManager.put("Panel.background", new Color(6,125,255,0));
            UIManager.put("ScrollablePane.background", new Color(6,125,255,0));
            UIManager.put("TabbedPane.contentOpaque", false);
            UIManager.put("Font", CurrentPlayer.defFont);
            
            UIManager.put("Label.font", CurrentPlayer.defFont);
            UIManager.put("TextArea.font", CurrentPlayer.defFont);
            UIManager.put("TextField.font", CurrentPlayer.defFont);
            UIManager.put("Button.font", CurrentPlayer.defFont);
            UIManager.put("TabbedPane.font", CurrentPlayer.defFont);
            UIManager.put("ComboBox.font", CurrentPlayer.defFont);
            UIManager.put("PasswordField.font", CurrentPlayer.defFont);
            //UIManager.put("TabbedPane.contentOpaque", false);
            //UIManager.put("ScrollPane.background", new Color(0, 0, 0, 20));
            //UIManager.put("TextField.background", new Color(0, 0, 0 ));
            //UIManager.put("TextField.foreground", new Color(255, 255, 255));
            //UIManager.put("PasswordField.background", new Color(0, 0, 0));
            //UIManager.put("PasswordField.foreground", new Color(255, 255, 255));
		} catch (ClassNotFoundException | InstantiationException
				| IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		   
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Login().setVisible(true);
            }
        });
        
		
	}
}
