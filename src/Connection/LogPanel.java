package Connection;
import javax.swing.*;

import Main.Player;
import Main.SocialFrame;

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.util.*;

public class LogPanel extends JPanel{
	private JLabel label1, label2, label3, toolbarLabel;
	private JPanel p1, p2, p3;
	private JTextField f1;
	private JPasswordField f2;
	private JButton b1;
	private JComboBox box1;
	private JFrame parentFrame;
	
	public LogPanel(JFrame parentFrame, JLabel toolbarLabel){
		this.parentFrame = parentFrame;
		this.toolbarLabel = toolbarLabel;

		label1 = new JLabel("Email");
		f1 = new JTextField(15);
		
		
		label2 = new JLabel("Password");
		f2 = new JPasswordField(15);
		
		
		b1 = new JButton("Log in");
		b1.setPreferredSize(new Dimension(100, 25));
		b1.addActionListener(new Listener());
		
		label3 = new JLabel("Languages");
		
		String[] languages = {"English", "Spanish", "Turkish"};
		box1 = new JComboBox(languages);
		
		
		GroupLayout layout = new GroupLayout(this);
		this.setLayout(layout);
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);
		
		layout.setHorizontalGroup(
		   layout.createSequentialGroup()
		   .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
			  .addComponent(label1)
		      .addComponent(label2)
		      .addComponent(label3))
		      .addGap(100, 100, 100)
		   .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
			  .addComponent(f1)
		      .addComponent(f2)
		      .addComponent(box1)
		      .addComponent(b1)));
		  
		layout.setVerticalGroup(
		   layout.createSequentialGroup()
		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
		           .addComponent(label1)
		           .addComponent(f1))
		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
		           .addComponent(label2)
		           .addComponent(f2))
		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
		           .addComponent(label3)
		           .addComponent(box1))
		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
		    	   .addComponent(b1))
		      
		);
	
		
	}
	
	private class Listener implements ActionListener{
		class LoginThread extends Thread{
			public void run(){
				Properties loginRes = null;
				
				loginRes = (Properties) PixelWordClient.readObject();
				
				
				if(loginRes.getProperty("tag").equals("success")){
					
					SocialFrame frame = new SocialFrame(Integer.parseInt(loginRes.getProperty("id")) , box1.getSelectedItem().toString().toLowerCase());
					//jLabel7.setText("Status : Logged in");
					parentFrame.dispose();
				}
				else{
					toolbarLabel.setText("Status : Login failed");
				}
			}
		}

	    public void actionPerformed(ActionEvent e){
	    
			if(f1.getText().equals("") | f2.getText().equals("")){
				toolbarLabel.setText("You have to fill both E-mail and Password fields");
			}
			else{
				
				Properties loginInformation = new Properties();
				loginInformation.put("tag", "LOGIN");
				loginInformation.put("email", f1.getText());
				loginInformation.put("password", f2.getText());
				
				PixelWordClient.sendObject(loginInformation);
				
				//PixelWordClient.sendData("LOGIN " + f1.getText() + " " + f2.getText() + " " + box1.getSelectedItem().toString());
				
	    		toolbarLabel.setText("Status: Waiting server to answer...");
				
				
				new LoginThread().start();
			}
	        
	    }
	}
	
}
