/*
 * Author : Cagla Gagaci
 * This part of project is completely a different part because it has nothing to do with the user. 
 * This java program should work on the server where MySql database is installed.
 */

package Connection;
import java.awt.*;
import java.io.*;
import java.net.*;

import java.sql.*;
import java.util.Properties;
import java.util.Scanner;

import javax.swing.*;

public class Server {

	private Connection con=null;
	private Statement statement=null;
	private String query=null;
	
	private JFrame frame;
	private JPanel panel;
	private ServerTextField text;
    /**
     * Runs the application. Pairs up clients that connect.
     */
    public Server() throws Exception {
    	final int port = 9090;//The port of communication between server and client
    	//MySQL database properties
    	final String serverAddress = "jdbc:mysql://localhost/pixelword";
    	final String dbUsername = "root";
    	final String dbPassword = "";
    	//Basic GUI to show server commands and outputs
    	frame = new JFrame("Server");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        
        panel = new JPanel();
        text = new ServerTextField();
        text.setPreferredSize(new Dimension(800,600));
        text.setEnabled(true);
        
        panel.add(text);
        
        frame.getContentPane().add(panel);
        frame.pack();
        frame.setVisible(true);
        
        frame.setState ( JFrame.ICONIFIED );//Automatically minimize on start
    	
        ServerSocket listener = new ServerSocket(port);//Our new ServerSocket
        /*
         * ServerSocket is an object for server-client communication to handle commands and create
         * an area to write outputs and read inputs
         */
        //text.append("PixelWord Server is Running"  );
        
        
        //MySql Connection
        try
        {
            //Driver addition.
            Class.forName("com.mysql.jdbc.Driver"); 
                 
            con=DriverManager.getConnection(serverAddress,dbUsername,dbPassword);
            
            statement=con.createStatement();//Statement created. It will provide us with execution of queries
            
            //text.append("Connection Establised! Mysql Server: " + serverAddress);
        }
        
        catch(SQLException e)
        {
            //text.append("Sql connection failed!" );
            System.exit(1);
        }
        catch(ClassNotFoundException e) 
        {
            //text.append("Sql driver failed!" );
            System.exit(1);
        }
        
        
        //Server Setup
        try {
        	/*
        	 * As Server is open many clients will try to connect to server.
        	 * This while loop will get all new clients when a new communication established.
        	 * Thread will be necessary to handle all these clients in the same time so Player class has to implement runnable
        	 * or extends Thread
        	 */
            while (true) {
                Player newPlayer = new Player(listener.accept());
                text.append("New Player is listening " + newPlayer.userIp);
                newPlayer.start();
            }
        } finally {
            listener.close();
        }
    }
    
    private class ServerTextField extends TextArea{
    	public ServerTextField(){
    		
    	}
    	
    	public void append(String s){//Append does not come with new line property. We needed to override it
    		super.append(s + "\n");
    	}
    }
    
    private class Player extends Thread {
    	//MySQL commands which can be called by clients
        private static final String LOGIN_REQUEST = "LOGIN";
		private static final String REGISTER_REQUEST = "REGISTER";
		private static final String GET_PLAYER_DETAILS = "GET_PLAYER_DETAILS";
		private static final String MATCH_SENT = "MATCH_SENT";
		private static final String GET_MATCHES = "GET_MATCHES";
		private static final String GET_FOLLOWS = "GET_FOLLOWS";
		private static final String ADD_FOLLOW = "ADD_FOLLOW";
		private static final String UN_FOLLOW = "UN_FOLLOW";
		private static final String IS_PLAYER = "IS_PLAYER";
		private static final String UPDATE_PLAYER = "UPDATE_PLAYER";
		char mark;
        Player opponent;
        Socket socket;
        String userIp;
        BufferedReader input;
        PrintWriter output;
        ObjectOutputStream oos;
        ObjectInputStream ois;

        /**
         * Constructs a handler thread for a given socket and mark
         * initializes the stream fields, displays the first two
         * welcoming messages.
         */
        public Player(Socket socket) {
            this.socket = socket;
            try {
            	//input is the reader which reads commands from clients
                input = new BufferedReader(
                    new InputStreamReader(socket.getInputStream()));
                //output is the write which writes commands to clients or generally answers of MySQL server
                output = new PrintWriter(socket.getOutputStream(), true);
                
                //oos = new ObjectOutputStream(socket.getOutputStream());
                
                //ois = new ObjectInputStream(socket.getInputStream());
                
                this.userIp = input.readLine();
                //We can directly read an input line which sends us the IP of client. We know that because our PixelWordClient sends it.
                text.append("Connected from : " + userIp  );
                output.println("hellooo there");
                	
            } catch (IOException e) {
                //text.append("Player died: " + userIp  );
            }
        }

        /*
         * Requested methods
         *  */
        public void loginRequested(Properties login) throws Exception {
        	//Query called
        	query = "SELECT * FROM users WHERE email='" + login.getProperty("email") + "' and password='" + login.getProperty("password") + "'";
        	//text.append("(Login User Control)Query called: " + query  );
        	
        	ResultSet rs = statement.executeQuery(query);
            //If there is a result this means there is a user with the specific password and email
        	Properties result = new Properties();
        	if(rs.next()){
        		//Sending the ID of user
        		result.put("tag", "success");
        		result.put("id", rs.getInt("id")+"");
        		//output.println("LOGIN_SUCCESS " + rs.getInt("id"));
        		//text.append("Login successful by " + rs.getString("email")  );
        	}
        	else{
        		result.put("tag", "fail");
        		//output.println("LOGIN_FAIL");
        		//text.append("Login Failed"  );
        	}
        	oos.writeObject(result);
        }
        
        public void regRequested( Properties register) throws SQLException, IOException, ClassNotFoundException {
        	
        	query = "SELECT * FROM users WHERE email='" + register.getProperty("email") + "'";
        	//text.append("(Existing user control)Query called: " + query  );
        	
        	ResultSet rs = statement.executeQuery(query);
            
        	if(rs.next()){
        		oos.writeObject(new String("REGISTER_FAIL"));
        		//text.append("Register Failed"  );
        	}
        	else{
        		query = "INSERT INTO users (email, password, alias, status) VALUES ('" + register.getProperty("email") + "','" + register.getProperty("password") + "','" + register.getProperty("alias")  + "','STATUS_NULL')";
        		statement.executeUpdate(query);
        		oos.writeObject(new String("REGISTER_SUCCESS"));
        		//text.append("Register Successful of " + register.getProperty("email")  );
        	}
        }
        
        public void detailsRequested(Properties detailsRequested) throws SQLException, IOException {
        	int ID = Integer.parseInt(detailsRequested.getProperty("id"));
        	
        	query = "SELECT * FROM users WHERE id='" + ID + "'";
        	//text.append("(Details Requested)Query called: " + query  );
        	
        	ResultSet rs = statement.executeQuery(query);
            
        	while(rs.next()){
        		Properties details = new Properties();
        		details.put("id", rs.getInt("id")+"");
        		details.put("email", rs.getString("email"));
        		details.put("alias", rs.getString("alias"));
        		details.put("status", rs.getString("status"));
        		
        		oos.writeObject(details);
        	}
        }
        
        public void matchSent(Properties match) throws SQLException, ClassNotFoundException, IOException {
        	
        	long currentTime = System.currentTimeMillis();
        	String currentTimeString = "" + currentTime;
        	query = "INSERT INTO matches (userid, language, score, time, guessedwords, letters) VALUES ('" + match.getProperty("userid") + "', '" + match.getProperty("language") + "', '" + match.getProperty("score") + "', '" + currentTimeString + "', '" + match.getProperty("words") + "', '" + match.getProperty("letters") + "')";
        	//text.append("(Score sent)Query called: " + query  );
        	
        	statement.executeUpdate(query);
           
        }
        
        public void getMatches(Properties matches) throws SQLException, IOException, ClassNotFoundException {
        	int size = 0;
        	
        	String querySize =  "SELECT count(*) FROM matches WHERE userid='" + matches.getProperty("id") + "' and language='" + matches.getProperty("language") + "'";
        	ResultSet rsSize = statement.executeQuery(querySize);
        	
        	while(rsSize.next())
    			size = rsSize.getInt(1);
        	
        	query = "SELECT * FROM matches WHERE userid='" + matches.getProperty("id") + "' and language='" + matches.getProperty("language") + "' ORDER BY time desc";
        	ResultSet rs = statement.executeQuery(query);
        	//text.append("(Scores Sending)Query called: " + query  );
            
        	Properties matchesCount = new Properties();
        	matchesCount.put("tag", "MATCHES_COUNT");
        	matchesCount.put("size", size+"");
        	oos.writeObject(matchesCount);
        	//text.append("SCORES_COUNT " + size);
        	
        	while(rs.next()){
        		Properties match = new Properties();
        		match.put("userid", rs.getInt("userid")+"");
        		match.put("score", rs.getInt("score")+"");
        		match.put("language", rs.getString("language"));
        		match.put("time", rs.getString("time"));
        		match.put("words", rs.getString("guessedwords"));
        		match.put("letters", rs.getString("letters"));
        		
        		oos.writeObject(match);
        		//output.println("MATCH " + rs.getInt("userid") + " " + rs.getInt("score") + " " + rs.getString("language") + " " + rs.getString("time") + " " + rs.getString("guessedwords") + " " + rs.getString("letters"));
        		//text.append("SCORE " + rs.getInt("userid") + " " + rs.getInt("score") + " " + rs.getString("language") + " " + rs.getString("time"));
        	}
        	
        }
        
        public void getFollows(Properties getFollows) throws SQLException, IOException {
        	int size = 0;
        	int ID = Integer.parseInt(getFollows.getProperty("id"));
        	
        	String querySize =  "SELECT count(*) FROM follows WHERE FollowerID='" + ID + "'";
        	ResultSet rsSize = statement.executeQuery(querySize);
        	
        	while(rsSize.next())
    			size = rsSize.getInt(1);
        	
        	query = "SELECT * FROM follows WHERE FollowerID='" + ID + "'";
        	ResultSet rs = statement.executeQuery(query);
        	//text.append("(Followers Sending)Query called: " + query  );
            
        	Properties followsCount = new Properties();
        	followsCount.put("tag", "FOLLOWS_COUNT");
        	followsCount.put("size", size+"");
        	oos.writeObject(followsCount);
        	//text.append("FOLLOWS_COUNT " + size);
        	
        	while(rs.next()){
        		Properties follow = new Properties();
        		follow.put("tag", "FOLLOW");
        		follow.put("id", rs.getInt("FollowedID") + "");
            	oos.writeObject(follow);
        		//text.append("FOLLOW " + rs.getInt("FollowedID"));
        	}
        	
        }
        
        public void addFollow(Properties addFollows) throws SQLException, IOException {
        	String followerEmail = addFollows.getProperty("followerEmail");
        	String followedEmail = addFollows.getProperty("followedEmail");
        	
    		query = "SELECT * FROM users WHERE email='" + followedEmail + "'";
    		ResultSet rs = statement.executeQuery(query);
    		Properties result = new Properties();
    		if(rs.next()){
    			query = "SELECT * FROM users WHERE email='" + followerEmail + "'";
        		ResultSet rs2 = statement.executeQuery(query);
        		rs2.next();
        		int followerID = rs2.getInt("id");
        		
        		query = "SELECT * FROM users WHERE email='" + followedEmail + "'";
        		rs2 = statement.executeQuery(query);
        		rs2.next();
        		int followedID = rs2.getInt("id");
        		
        		query = "SELECT * FROM follows WHERE FollowerID='" + followerID + "' and FollowedID='" + followedID +"'";
        		rs2 = statement.executeQuery(query);
        		if(!rs2.next()){
        			query = "INSERT INTO follows (FollowerID, FollowedID) VALUES ('" + followerID + "','" + followedID + "')" ;
            		statement.executeUpdate(query);
            		result.put("tag", "ADDED_FOLLOW");
        		}
        		else{
        			result.put("tag", "ALREADY_FOLLOWING");
        		}
        		
    		}
    		else{
    			result.put("tag", "NO_ACCOUNT");
    		}
    		oos.writeObject(result);
        }
        
        public void unFollow(Properties unFollow) throws SQLException, IOException {
        	
        	int followerID = Integer.parseInt( unFollow.getProperty("followerEmail") );
        	int followedID = Integer.parseInt( unFollow.getProperty("followedEmail") );
        	
    		query = "DELETE FROM follows WHERE FollowerID = '" + followerID + "' and FollowedID = '" + followedID + "'";
    		//text.append("(Unfollow Requested)Query: " + query);
    		statement.executeUpdate(query);
    		Properties result = new Properties();
    		result.put("tag", "SUCCESS");
    		oos.writeObject(result);
    	
        }
        
        public void isPlayer(Properties isPlayer) throws SQLException, IOException {
        	String playerEmail = isPlayer.getProperty("email");
        	
    		query = "SELECT * FROM users WHERE email='" + playerEmail + "'";
    		ResultSet rs = statement.executeQuery(query);
    		Properties result = new Properties();
    		if(rs.next()){
    			result.put("tag", "PLAYER_DETAILS");
    			result.put("id", rs.getInt("id")+"");
    			//text.append("New Player found by Email " + rs.getInt("id"));
    		}
    		else{
    			result.put("tag", "NO_ACCOUNT");
    		}
    		oos.writeObject(result);
        }
        
        public void updatePlayer(Properties update) throws SQLException, ClassNotFoundException, IOException {
        	
        	PreparedStatement ps;
        	if(!update.getProperty("password").equals("DONT_SET_PASSWORD_PIXELWORD")){
        		ps = con.prepareStatement("UPDATE users SET password = ?, alias = ?, status = ? WHERE id = ?");

    		    ps.setString(1,update.getProperty("password"));
    		    ps.setString(2,update.getProperty("alias"));
    		    ps.setString(3,update.getProperty("status"));
    		    ps.setInt(4, Integer.parseInt(update.getProperty("id")));
        	}
        	else{
        		ps = con.prepareStatement("UPDATE users SET alias = ?, status = ? WHERE id = ?");
        		
        		ps.setString(1,update.getProperty("alias"));
    		    ps.setString(2,update.getProperty("status"));
    		    ps.setInt(3, Integer.parseInt(update.getProperty("id")));
        	}
    		

		    ps.executeUpdate();
		    ps.close();
        	
		    Properties result = new Properties();
		    result.put("tag", "UPDATE_SUCCESS");
		    oos.writeObject(result);
		    //text.append("Update Successful on ID: " + update.getProperty("id"));
        }
        
        

        /**
         * The run method of this thread.
         */
        public void run() {
            try {
            	
                while (true) {
                    Properties props = (Properties) ois.readObject();
                    if( props.getProperty("tag").equals(LOGIN_REQUEST) ){
                    	loginRequested(props);
                    }
                    else if( props.getProperty("tag").equals(REGISTER_REQUEST) ){
                    	regRequested(props);
                    }
                    else if( props.getProperty("tag").equals(GET_PLAYER_DETAILS)){
                    	detailsRequested(props);
                    }
                    else if( props.getProperty("tag").equals(MATCH_SENT)){
                    	matchSent(props);
                    }
                    else if( props.getProperty("tag").equals(GET_MATCHES)){
                    	getMatches(props);
                    }
                    else if( props.getProperty("tag").equals(GET_FOLLOWS)){
                    	getFollows(props);
                    }
                    else if( props.getProperty("tag").equals(ADD_FOLLOW)){
                    	addFollow(props);
                    }
                    else if( props.getProperty("tag").equals(UN_FOLLOW)){
                    	unFollow(props);
                    }
                    else if( props.getProperty("tag").equals(IS_PLAYER)){
                    	isPlayer(props);
                    }
                    else if( props.getProperty("tag").equals(UPDATE_PLAYER)){
                    	updatePlayer(props);
                    }
                }
            } catch (Exception e) {
                //text.append("Player died: " + userIp  );
            } finally {
                try {socket.close();} catch (IOException e) {}
            }
        }
    }
    public static void main(String[] args) throws Exception{
    	Server server = new Server();
    	//Starting the server
    }
}

