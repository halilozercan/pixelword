package Connection;
import javax.swing.*;

import WordGame.LanguageMethods;


import java.awt.*;
import java.awt.event.*;
import java.util.Properties;

public class RegPanel extends JPanel implements ActionListener{
	private JLabel label1, label2, label3, label4, label5, label6, toolbarLabel;
	private JPanel p1, p2, p3, p4, p5, p6, p7;
	private JTextField f1, f4, f5, f6;
	private JPasswordField f2, f3;
	private JButton b1;
	private CaptchaPanel captchaPanel;
	
	public RegPanel(JLabel toolbarLabel){
		this.toolbarLabel = toolbarLabel;

		label1 = new JLabel("Email");
		f1 = new JTextField(15);
		
		label2 = new JLabel("Password");
		f2 = new JPasswordField(15);
		
		label3 = new JLabel("Re Password");
		f3 = new JPasswordField(15);
		
		label4 = new JLabel("Alias");
		f4 = new JTextField(15);
		
		captchaPanel = new CaptchaPanel();
		f5 = new JTextField(6);
		
		b1 = new JButton("Register");
		b1.addActionListener(this);
		
		
		GroupLayout layout = new GroupLayout(this);
		this.setLayout(layout);
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);
		
		layout.setHorizontalGroup(
		   layout.createSequentialGroup()
		   .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
		      .addComponent(label1)
		      .addComponent(label2)
		      .addComponent(label3)
		      .addComponent(label4)
		      .addComponent(captchaPanel))
		      .addGap(100, 100, 100)
		   .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
			  .addGap(100, 100, 100)
			  .addComponent(f1)
		      .addComponent(f2)
		      .addComponent(f3)
		      .addComponent(f4)
		      .addComponent(f5)
		      .addComponent(b1)));
		  
		layout.setVerticalGroup(
		   layout.createSequentialGroup()
		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
		           .addComponent(label1)
		           .addComponent(f1))
		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
		           .addComponent(label2)
		           .addComponent(f2))
		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
		           .addComponent(label3)
		           .addComponent(f3))
		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
		           .addComponent(label4)
		           .addComponent(f4))
		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
		           .addComponent(captchaPanel)
		           .addComponent(f5))     

		      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
		           .addComponent(b1)
		           
		));
		
	}
	
	class RegisterThread extends Thread{
    	public void run(){
			String readData = (String) PixelWordClient.readObject();
			
			if(readData.equals("REGISTER_SUCCESS")){
				//start the game
				toolbarLabel.setText("Register is done. Now you can log in");
			}
			else{
				toolbarLabel.setText("Cannot register due to same email");
			}
    	}
    }

    public void actionPerformed(ActionEvent e){
    	
		if(!f2.getText().equals(f3.getText())){
			toolbarLabel.setText("Cannot register , passwords doesnt match");
		}
		else if(f1.getText().equals("") | f2.getText().equals("") | f3.getText().equals("") | f4.getText().equals("")){
			toolbarLabel.setText("You need to fill all fields");
		}
		else if(!IsValidEmail(f1.getText())){
			toolbarLabel.setText("Your e-mail does not seem to be valid");
		}
		else if(!f5.getText().equals(captchaPanel.randomLetters)){
			toolbarLabel.setText("Captcha is not correct");
		}
		else{
			Properties register = new Properties();
			register.put("tag", "REGISTER");
			register.put("email", f1.getText());
			register.put("password" , f2.getText());
			register.put("alias", f4.getText());
			
			PixelWordClient.sendObject(register);
		
			toolbarLabel.setText("Status: Waiting server to answer...");
			
			new RegisterThread().start();
			
		}
    }
    
    public static boolean IsValidEmail(String email){
    	if(email.indexOf('.')==-1 | email.indexOf('@')==-1)
    		return false;
    	else if(email.lastIndexOf('.') < email.length()-5)
    		return false;
    	else if(email.length() > 50 | email.length()<14)
    		return false;
    	return true;
    }
    
    private class CaptchaPanel extends JPanel{
    	public String randomLetters;
    	public CaptchaPanel(){
    		randomLetters = LanguageMethods.getRandomLetters(6, "english", 0, 0).toString();
    		for(int i = 0 ; i < randomLetters.length(); i++ ){
    			JLabel label = new JLabel("" , new ImageIcon("Alphabet/" + randomLetters.charAt(i) + ".png"), JLabel.LEFT);
    			this.add(label);
    		}
    	}
    }
    	
   
}
