package Connection;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class OffPanel extends JPanel{
	private JButton b1;
	private JPanel p1, p2, p3;
	
	
	public OffPanel(){
		
		setPreferredSize(new Dimension(450, 225));
		
		setLayout(new BorderLayout());
		
		p1 = new JPanel();
		p1.setPreferredSize(new Dimension(350, 50));
		
		p2 = new JPanel();
		p2.setPreferredSize(new Dimension(400, 50));
		b1 = new JButton("Choose offline and continue to play and upload your scores later");
		b1.setPreferredSize(new Dimension(400, 25));
		b1.addActionListener(new Listener());
		p2.add(b1);
		
		p3 = new JPanel();
		p3.setPreferredSize(new Dimension(350, 50));
		
		add(p1, BorderLayout.NORTH);
		add(p2, BorderLayout.CENTER);
		add(p3, BorderLayout.SOUTH);
	}
	
	private class Listener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			
		}
	}
}
