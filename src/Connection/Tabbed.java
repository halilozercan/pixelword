/*
 * Author: Safa Onur Sahin
 * Tabbed class consists of login,register and offline panels and serves them as tabbedPane
 */
package Connection;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JToolBar;

import javax.swing.*;

import Main.SocialFrame;

/**
 *
 * @author absolute
 */
public class Tabbed extends JPanel{

    /**
     * Creates new form Tabbed
     */
	private JTabbedPane jTabbedPane1;  
	private JFrame parentFrame;
	private JToolBar toolbar;
	private JLabel toolbarLabel;
    public Tabbed(JFrame parentFrame) {
    	

        this.parentFrame = parentFrame;
    	setLayout(new BorderLayout());
    	initComponents();
        
     // 
    }
    
    public void paintComponent(Graphics g){
		g.drawImage( new ImageIcon("bg.jpg").getImage(),0,0,null);
	}
   
    
    
    private void initComponents() {

        jTabbedPane1 = new JTabbedPane();
        toolbarLabel = new JLabel("Welcome to PixelWord!");
		toolbar = new JToolBar();
		toolbar.add(toolbarLabel);

        jTabbedPane1.addTab("Log In", new LogPanel(parentFrame, toolbarLabel));
        jTabbedPane1.addTab("Register", new RegPanel(toolbarLabel));
        jTabbedPane1.addTab("Offline", new OffPanel());
        add(jTabbedPane1, BorderLayout.CENTER);
        add(toolbar, BorderLayout.SOUTH);
        
        if( !PixelWordClient.connectStatus){
        	jTabbedPane1.setEnabledAt(0, false);
        	jTabbedPane1.setEnabledAt(1, false);
        	jTabbedPane1.setSelectedIndex(2);
        }
    }// </editor-fold>                            

                
}

