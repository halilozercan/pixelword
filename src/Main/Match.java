/*
 * Author: Kaan Aky�z
 * 
 */
package Main;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.MouseInputAdapter;

import WordGame.GameAnagram;
import WordGame.LanguageMethods;

public class Match implements Serializable{
	
	private Player p;
	private int score;
	private String language;
	private long dateTime;
	private ArrayList<Integer> guessedWords;
	private String letters;
	
	public Match(Player p, int score, String language, long dateTime){
		
		this.p = p;
		this.score = score;
		this.language = language;
		this.dateTime = dateTime;
		this.guessedWords = new ArrayList<Integer>();
	}
	
	
	public Player getUser(){
		return p;
	}
	
	public int getScore(){
		return score;
	}
	public String getLanguage(){
		return language;
	}
	public String getDateTime(){
		if(dateTime == 0)
			return "Never Played";
		String dateTimeString;
		java.text.SimpleDateFormat sdf = 
       	     new java.text.SimpleDateFormat("dd/MMMM/yyyy' 'hh:mm");
		Timestamp timestamp = new Timestamp(dateTime);
		return sdf.format(timestamp);
	}
	public String getLetters(){
		return letters;
	}
	public ArrayList<Integer> getGuessedWords(){
		return guessedWords;
	}
	
	public String toString(){
		return ""+this.score;
	}
	
	public void addGuessedWord(int a){
		guessedWords.add(a);
	}
	
	public void addGuessedWordList(String list){
		Scanner scan = new Scanner(list);
		while(scan.hasNext()){
			addGuessedWord(scan.nextInt());
		}
	}
	public void setLetters(String let){
		letters = let;
	}
	
	public void getFrameOfMatch(){
    	
		JFrame frame = new JFrame("Game Summary");
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		JPanel matchPanel = new JPanel();
		matchPanel.setLayout(new BoxLayout(matchPanel, BoxLayout.PAGE_AXIS));
		
		JLabel user = new JLabel(this.p.getAlias());
		JLabel score = new JLabel("Score: " + this.score + "");
		JLabel date = new JLabel("Date " + getDateTime());
		JLabel letters = new JLabel("Letters " + this.letters);
		JPanel guessedWordsPanel = new JPanel();
		guessedWordsPanel.setBackground(new Color(255,255,255,50));
		guessedWordsPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		guessedWordsPanel.setPreferredSize(new Dimension(300,200));
		for(int i = 0 ; i < this.guessedWords.size() ; i++ ){
			JLabel word = new JLabel(new GameAnagram(language).getWordOfID( guessedWords.get(i) ) );
			word.setForeground( getRandomColor() );
			guessedWordsPanel.add( word );
		}
		matchPanel.add(user);
		matchPanel.add(score);
		matchPanel.add(date);
		matchPanel.add(letters);
		matchPanel.add(guessedWordsPanel);
		matchPanel.setBorder(new EmptyBorder(10,10,10,10));
		matchPanel.addMouseListener(new DragListener());
		
		frame.setLocationRelativeTo(null);
		
		//javax.swing.SwingUtilities.getWindowAncestor( matchPanel).setEnabled( false);
		
		frame.getContentPane().add(matchPanel);
		frame.pack();
		frame.setVisible(true);
		
	}
	
	public JPanel getMiniPanel(){
		JPanel result = new JPanel();
		result.setLayout(new BoxLayout(result, BoxLayout.X_AXIS));
		JLabel user = new JLabel(this.p.getAlias());
		JLabel score = new JLabel("Score: " + this.score + "");
		result.add(user);
		result.add(Box.createHorizontalGlue());
		result.add(score);
		
		return result;
		
		
	}
	
	public class DragListener extends MouseInputAdapter {
    	
    	private int currentX, currentY;
    	
    	@Override
    	public void mousePressed( MouseEvent e) {
    		
    		currentX = e.getX();
    		currentY = e.getY();
    	}
    	
    	@Override
    	public void mouseDragged( MouseEvent e) {
    		
    		((Window)e.getSource()).setLocation( e.getXOnScreen() - currentX, e.getYOnScreen() - currentY);
    	}
    }
	
	public static Color getRandomColor(){
		Random gen = new Random();
		int r = gen.nextInt(3) * 127;
		int g = gen.nextInt(3) * 127;
		int b = gen.nextInt(3) * 127;
		return new Color( r, g ,b ) ;
	}
}
