package Main;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.AbstractAction;
import javax.swing.border.LineBorder;
import javax.swing.event.MouseInputAdapter;


public class Popup extends JFrame {
    JButton closeButton;
    ImageIcon closeImage;
    ImageIcon pressedImage;
    ImageIcon enteredCloseImage;
    public int width;
    public int height;
    private JPanel parentPanel;
       
    //CONSTRUCTORS
    public Popup()
    {
    	super();
        width = 490;
        height = 420;
        init();
    }
    
    public Popup( JPanel parent) {
    	
    	super();
        width = 490;
        height = 420;
    	init();
    	setParentPanel( parent);
    }
    
    public Popup( JPanel parent, int width, int height) {
    	
    	super();
    	this.width = width;
    	this.height = height;
    	init();
    	setParentPanel( parent);
    }
    
    //METHODS
    public void init() {
    	
        closeImage = new ImageIcon( "close.png");
        pressedImage = new ImageIcon("closePressed.png");
        enteredCloseImage = new ImageIcon( "closeEntered.png");
        
        parentPanel = null;
              	
    	closeButton = new JButton(closeImage);
    	closeButton.setPressedIcon(pressedImage);
    	closeButton.setMargin( new Insets( 0, 0, 0, 0));
		closeButton.setBorder( BorderFactory.createEmptyBorder());
		closeButton.addActionListener(new CloseButtonActionListener());
		closeButton.addMouseListener(new CloseButtonActionListener());
       	
       	this.setLayout(new BorderLayout());
    	JPanel closeButtonPanel = new JPanel();
    	closeButtonPanel.setLayout(new BorderLayout());
    	closeButtonPanel.add(closeButton,BorderLayout.EAST);
    	this.add(closeButtonPanel,BorderLayout.NORTH);
    	
    	((JComponent)getContentPane()).setBorder(   
            BorderFactory.createMatteBorder( 1, 1, 1, 1, new Color(73,136,205) ) );
		Toolkit tk = Toolkit.getDefaultToolkit();
		Dimension screenSize = tk.getScreenSize();	
       	setUndecorated(true);
    	setVisible(true);
    	setBounds( screenSize.width / 2 - width / 2, screenSize.height / 2 - height / 2, width, height);
    	setResizable(false);
    	
    	DragListener dragListener = new DragListener();
    	addMouseListener( dragListener);
    	addMouseMotionListener( dragListener);
    }
    
    public void enableFrame() {
    	
    	if( parentPanel != null)
    		javax.swing.SwingUtilities.getWindowAncestor( parentPanel).setEnabled( true);
    }
    
    public void disableFrame() {
    	
    	if( parentPanel != null)
    		javax.swing.SwingUtilities.getWindowAncestor( parentPanel).setEnabled( false);
    }
    
    public void setParentPanel( JPanel panel) {
    	
    	parentPanel = panel;
    	disableFrame();
    }
    
    public JPanel getParentPanel() {
    	
    	return parentPanel;
    }
    
    public void close() {
    	
    	enableFrame();
    	dispose();
    }
    
    public class CloseButtonActionListener implements ActionListener, MouseListener
    {
    	public void actionPerformed(ActionEvent e)
    	{
    		
    		enableFrame();
    		Popup.this.dispose();
    	}
    	
    	public void mouseClicked(MouseEvent e){}
    	public void mousePressed(MouseEvent e){}
		public void mouseReleased(MouseEvent e){}
			
		public void mouseEntered(MouseEvent e){
			closeButton.setIcon(enteredCloseImage);
		}
		
		public void mouseExited(MouseEvent e){
			closeButton.setIcon(closeImage);
		}
		
    }
    
    public class DragListener extends MouseInputAdapter {
    	
    	private int currentX, currentY;
    	
    	@Override
    	public void mousePressed( MouseEvent e) {
    		
    		currentX = e.getX();
    		currentY = e.getY();
    	}
    	
    	@Override
    	public void mouseDragged( MouseEvent e) {
    		
    		((Window)e.getSource()).setLocation( e.getXOnScreen() - currentX, e.getYOnScreen() - currentY);
    	}
    }
    
}
