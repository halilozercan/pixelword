/*
 * Author: Safa Onur Sahin
 * 
 */
package Main;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.UIManager;

import WordGame.LanguageMethods;
public class SocialFrame {

	private int ID;
	public SocialFrame(int ID, String preferredLanguage){
		this.ID = ID;
		
		CurrentPlayer.preferredLanguage = preferredLanguage;
		CurrentPlayer.player = new Player(ID);
		CurrentPlayer.allWords = LanguageMethods.getAllWords(preferredLanguage);
		
		JFrame frame = new JFrame("PixelWord - Developer Version");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		UIManager.put("TabbedPane.contentOpaque", false);
		
		frame.getContentPane().add(new Profile(CurrentPlayer.player));
		frame.pack();
		frame.setVisible(true);
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
   	 
    	// Determine the new location of the window
    	int w = frame.getSize().width;
    	int h = frame.getSize().height;
    	int x = (dim.width-w)/2;
    	int y = (dim.height-h)/2;
    	 
    	// Move the window
    	frame.setLocation(x, y);

	}

}
