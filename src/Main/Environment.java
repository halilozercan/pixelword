/*
 * Author: Safa Onur Sahin
 * 
 */
package Main;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.*;

public class Environment extends JTabbedPane implements Updatable{
	
	private Player p;
	private JPanel lastScores, highScores, follows;
	private Profile parentProfile;
	public Environment(Player p, Profile parentProfile){
		this.setPreferredSize(new Dimension(750,400));
		this.p = p;
		
		this.parentProfile = parentProfile;
		initComponents();
	}
	
	public void initComponents(){
		this.lastScores = new LastScores(p);
		this.highScores = new HighScores(p);
		this.follows    = new Follows(p);

		JScrollPane lastScoresScroller = new JScrollPane(lastScores);
		lastScoresScroller.setOpaque(false);
		lastScoresScroller.getViewport().setOpaque(false);
		lastScoresScroller.setBorder(BorderFactory.createEmptyBorder());
		JScrollPane highScoresScroller = new JScrollPane(highScores);
		highScoresScroller.setOpaque(false);
		highScoresScroller.getViewport().setOpaque(false);
		highScoresScroller.setBorder(BorderFactory.createEmptyBorder());
		
		this.addTab("Follows"    , follows);
		this.addTab("Last Scores", lastScoresScroller);
		this.addTab("High Scores", highScoresScroller);
	}
	
	private class LastScores extends JPanel{
		
		public LastScores(Player p){
			this.setPreferredSize(new Dimension(750,400));
			ArrayList<Match> lastScores = p.getLastMatchesFromFollows();

			for(int i = 0 ; i < lastScores.size() ; i++ ){
				JPanel container = new JPanel();
				container.setPreferredSize(new Dimension(725,40));
		
				JPanel aliasPanel = new JPanel();
				JLabel aliasLabel = new JLabel(lastScores.get(i).getUser().getAlias());
				
				//aliasLabel.setFont(new Font("Calibri", Font.BOLD, 24));
				aliasLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
				aliasLabel.addMouseListener(new ProfileListener(lastScores.get(i).getUser().getID()));
				//aliasPanel.setBorder(BorderFactory.createRaisedBevelBorder());
				aliasPanel.setLayout(new BoxLayout(aliasPanel, BoxLayout.Y_AXIS));
				aliasPanel.setPreferredSize(new Dimension(250,40));
				aliasPanel.add(aliasLabel);
				
				JPanel datePanel = new JPanel();
				JLabel dateLabel = new JLabel(""+lastScores.get(i).getDateTime());
				
				//dateLabel.setFont(new Font("Calibri", Font.BOLD, 24));
				dateLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
				dateLabel.setForeground(Color.gray);
				//datePanel.setBorder(BorderFactory.createRaisedBevelBorder());
				datePanel.setLayout(new BoxLayout(datePanel, BoxLayout.Y_AXIS));
				datePanel.setPreferredSize(new Dimension(250,40));
				datePanel.add(dateLabel);
				
				JPanel scorePanel = new JPanel();
				JLabel scoreLabel = new JLabel(""+lastScores.get(i));
				
				//scoreLabel.setFont(new Font("Calibri", Font.BOLD, 24));
				scoreLabel.setForeground(Color.red);
				scoreLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
				scoreLabel.addMouseListener(new MatchListener(lastScores.get(i)));
				//scorePanel.setBorder(BorderFactory.createLoweredBevelBorder());
				scorePanel.setPreferredSize(new Dimension(200,40));
				scorePanel.add(scoreLabel);
				
				container.add(aliasPanel);
				container.add(datePanel);
				container.add(scorePanel);
				add(container);
			}
			
			
		}
	}
	
	private class HighScores extends JPanel{
		
		public HighScores(Player p){
			
			ArrayList<Match> highScores = p.getHighMatchesFromFollows();
			this.setPreferredSize(new Dimension(750,400));
			
			
			for(int i = 0 ; i < highScores.size() ; i++ ){
				
				JPanel container = new JPanel();
				container.setPreferredSize(new Dimension(725,40));
				
				JPanel aliasPanel = new JPanel();
				JLabel aliasLabel = new JLabel(highScores.get(i).getUser().getAlias());
				
				//aliasLabel.setFont(new Font("Calibri", Font.BOLD, 24));
				aliasLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
				aliasLabel.addMouseListener(new ProfileListener(highScores.get(i).getUser().getID()));
				//aliasPanel.setBorder(BorderFactory.createRaisedBevelBorder());
				aliasPanel.setLayout(new BoxLayout(aliasPanel, BoxLayout.Y_AXIS));
				aliasPanel.setPreferredSize(new Dimension(250,40));
				aliasPanel.add(aliasLabel);
				
				JPanel datePanel = new JPanel();
				JLabel dateLabel = new JLabel(""+highScores.get(i).getDateTime());
				
				//dateLabel.setFont(new Font("Calibri", Font.BOLD, 24));
				dateLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
				dateLabel.setForeground(Color.gray);
				//datePanel.setBorder(BorderFactory.createRaisedBevelBorder());
				datePanel.setLayout(new BoxLayout(datePanel, BoxLayout.Y_AXIS));
				datePanel.setPreferredSize(new Dimension(250,40));
				datePanel.add(dateLabel);
				
				JPanel scorePanel = new JPanel();
				JLabel scoreLabel = new JLabel(""+highScores.get(i));
				
				//scoreLabel.setFont(new Font("Calibri", Font.BOLD, 24));
				scoreLabel.setForeground(Color.red);
				scoreLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
				scoreLabel.addMouseListener(new MatchListener(highScores.get(i)));
				//scorePanel.setBorder(BorderFactory.createLoweredBevelBorder());
				scorePanel.setPreferredSize(new Dimension(200,40));
				scorePanel.add(scoreLabel);
				
				container.add(aliasPanel);
				container.add(datePanel);
				container.add(scorePanel);
				add(container);
			}
		}
	}
	private class Follows extends JPanel{
		
		public Follows(Player p){
			
			ArrayList<Player> follows = new ArrayList();
			this.setPreferredSize(new Dimension(750,400));
			setLayout(new GridLayout(4,0));
			
			for(int i = 0 ; i < p.follows.size() ; i++ ){
				Player tempPlayer = new Player(p.follows.get(i));
				add(tempPlayer.getMiniPanelLinked(new ProfileListener(tempPlayer.getID())));
			}
		}
	}
	
	private class ProfileListener extends MouseAdapter{
		Player p;
		public ProfileListener(int ID){
			this.p = new Player(ID);
		}
		public void mouseClicked(MouseEvent e){
			parentProfile.getCurrentProfile().update(p);
		
		}
		
		public void mouseEntered(MouseEvent e) {
		     Cursor cursor = Cursor.getDefaultCursor();
		     cursor = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR); 
		     setCursor(cursor);
		  }
		public void mouseExited(MouseEvent e) {
		     Cursor cursor = Cursor.getDefaultCursor();
		     cursor = Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR); 
		     setCursor(cursor);
		  }
	}
	
	private class MatchListener extends MouseAdapter{
		Match m;
		public MatchListener(Match m){
			this.m = m;
		}
		public void mouseClicked(MouseEvent e){
			
			m.getFrameOfMatch();
		}
		
		public void mouseEntered(MouseEvent e) {
		     Cursor cursor = Cursor.getDefaultCursor();
		     cursor = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR); 
		     setCursor(cursor);
		 
		}
		public void mouseExited(MouseEvent e) {
		     Cursor cursor = Cursor.getDefaultCursor();
		     cursor = Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR); 
		     setCursor(cursor);
		}
	}
	public void update(Player p){
		this.p = p;
		this.removeAll();
		initComponents();
	}
}
