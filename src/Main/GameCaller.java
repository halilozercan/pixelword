/*
 * Author: Kaan Aky�z
 * 
 */
package Main;
import javax.swing.JFrame;
import GamePlay.*;
public class GameCaller {

	private String language;
	private int gameMode;
	private Profile profile;
	
	public GameCaller(String language, Profile profile) {
		this.profile = profile;
		
		JFrame frame = new JFrame("Anagram Play!");
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		final Game newGame = new Game(language, frame, profile);
		
		//frame.setUndecorated(true);
		frame.getContentPane().add(newGame);
		frame.pack();
		frame.setVisible(true);
		
		
	}

}
