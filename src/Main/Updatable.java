package Main;

public interface Updatable {
	public void update(Player p);
}
