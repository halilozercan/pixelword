/*
 * Author: Safa Onur Sahin
 * 
 */
package Main;

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.util.*;

import javax.swing.*;

import Connection.PixelWordClient;

public class EditProfile extends JFrame{
	Player p;
	CurrentProfile parentCurrentProfile;
	public EditProfile(Player p, CurrentProfile parentProfilePanel){
		this.p = p;
		this.parentCurrentProfile = parentProfilePanel;
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.getContentPane().add(new EditProfilePanel(this));
		this.pack();
		this.setVisible(true);
	}
	private class EditProfilePanel extends JPanel{
		JLabel l1,l2,l3,l4,l5, toolbarLabel;
		JTextField eMail, alias, status;
		JPasswordField password1, password2;
		JButton submit;
		JToolBar toolbar;
		JFrame parentFrame;
		public EditProfilePanel(JFrame parentFrame){
			this.parentFrame = parentFrame;
			this.setLayout(new GridLayout(10,2));
			
			l1 = new JLabel("E-mail");
			eMail = new JTextField(p.getEmail());
			eMail.setEnabled(false);
			this.add(l1);
			this.add(eMail);
			
			l2 = new JLabel("New Password");
			password1 = new JPasswordField(32);
			password1.addKeyListener(new TextListener());
			this.add(l2);
			this.add(password1);
			
			l3 = new JLabel("New Password Again");
			password2 = new JPasswordField(32);
			password2.addKeyListener(new TextListener());
			this.add(l3);
			this.add(password2);
			
			l4 = new JLabel("Alias");
			alias = new JTextField(p.getAlias());
			alias.addKeyListener(new TextListener());
			this.add(l4);
			this.add(alias);
			
			l5 = new JLabel("Status");
			if(p.getStatus().equals("STATUS_NULL"))
				status = new JTextField();
			else
				status = new JTextField(p.getStatus());
			this.add(l5);
			this.add(status);
			
			submit = new JButton("Submit");
			submit.addActionListener(new UpdateProfileListener());
			this.add(new JPanel());
			this.add(submit);
			
			toolbar = new JToolBar();
			toolbarLabel = new JLabel("Edit Profile...");
			toolbar.add(toolbarLabel);
			this.add(toolbar);
		}
		private class TextListener implements KeyListener{
			public void keyPressed(KeyEvent e){
			
			}
			public void keyTyped(KeyEvent e){
				
			}
			@SuppressWarnings("deprecation")
			public void keyReleased(KeyEvent e){
				
				if(!password1.getText().equals(password2.getText())){
					submit.setEnabled(false);
					toolbarLabel.setText("Passwords Does Not Match");
				}
				else if( alias.getText().contains(" ")){
					toolbarLabel.setText("Alias has to be one word without any space");
					submit.setEnabled(false);
				}
				else{
					toolbarLabel.setText("Edit Profile...");
					submit.setEnabled(true);
				}
			}
		}
		private class UpdateProfileListener implements ActionListener{
			public void actionPerformed(ActionEvent e){
				final JFrame confirmFrame = new JFrame("Confirm Your Password");
				confirmFrame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
				
				final JPanel confirmPanel 			= new JPanel();
				final JLabel confirmLabel 			= new JLabel("Type your password to confirm changes");
				final JPasswordField confirmField	= new JPasswordField(32);
				final JButton confirmButton 		= new JButton("Ok");
				final JButton cancelButton			= new JButton("Cancel");
				
				confirmButton.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent e){
						
						Properties login = new Properties();
						login.put("tag", "LOGIN");
						login.put("email", p.getEmail());
						login.put("password", confirmField.getText());
						
						PixelWordClient.sendObject(login);
						
						Properties loginResult = (Properties) PixelWordClient.readObject();
						
						if(loginResult.getProperty("tag").equals("success")){
							
							
							Properties update = new Properties();
							update.put("tag", "UPDATE_PLAYER");
							update.put("id", p.getID());
							if(!password1.getText().equals(""))
								update.put("password", password1.getText());
							else
								update.put("password", "DONT_SET_PASSWORD_PIXELWORD");
							update.put("alias", alias.getText());
							update.put("status", status.getText());
							
							PixelWordClient.sendObject(update);
							
							Properties updateResult = (Properties) PixelWordClient.readObject();
							
							if(updateResult.getProperty("tag").equals("UPDATE_SUCCESS")){
								
								JOptionPane.showMessageDialog(null, "Profile Updated");
								confirmFrame.dispose();
								toolbarLabel.setText("Profile Updated...");
								
								CurrentPlayer.player = new Player(p.getID());
								parentCurrentProfile.parentProfile.update(CurrentPlayer.player);
								parentCurrentProfile.parentProfile.update(CurrentPlayer.player);
								parentFrame.dispose();
							}
						}
						else{
							JOptionPane.showMessageDialog(null, "Wrong Password");
						}
					}
				});
				cancelButton.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent e){
						confirmFrame.dispose();
					}
				});
				
				confirmPanel.add(confirmLabel);
				confirmPanel.add(confirmField);
				confirmPanel.add(confirmButton);
				confirmPanel.add(cancelButton);
				
				confirmFrame.getContentPane().add(confirmPanel);
				confirmFrame.pack();
				confirmFrame.setVisible(true);
			}
		}
	}
}
