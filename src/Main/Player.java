/*
 * Author: Kaan Aky�z
 * 
 */
package Main;
import Connection.*;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

public class Player{
	private int ID;
	private String alias;
	private String email;
	private String status;
	public ArrayList<Match> matches;
	public ArrayList<Integer> follows;
	
	public Player(int ID){
		int size;
		this.ID = ID;
		String readData = null;
		String matchString, followString;
		matches = new ArrayList<Match>();
		follows = new ArrayList<Integer>();
		
		
		Properties getDetails = new Properties();
		getDetails.put("tag", "GET_PLAYER_DETAILS");
		getDetails.put("id", this.ID+"");
		PixelWordClient.sendObject(getDetails);
		
		Properties details = (Properties)PixelWordClient.readObject();
		
		this.ID = Integer.parseInt(details.getProperty("id"));
		this.email = details.getProperty("email");
		this.alias = details.getProperty("alias");
		this.status = details.getProperty("status");
		
		Properties getMatches = new Properties();
		getMatches.put("tag", "GET_MATCHES");
		getMatches.put("id", this.ID + "");
		getMatches.put("language", CurrentPlayer.preferredLanguage);
		PixelWordClient.sendObject(getMatches);
		
		Properties matchesCount = (Properties) PixelWordClient.readObject();
		
		if(matchesCount.getProperty("tag").equals("MATCHES_COUNT")){
			
			for( int i = 0 ; i < Integer.parseInt(matchesCount.getProperty("size")) ; i++ ){
				
				Properties match = (Properties)PixelWordClient.readObject();
				
				Match m = new Match(this, Integer.parseInt(match.getProperty("score")), match.getProperty("language"), Long.parseLong(match.getProperty("time")) );
				m.addGuessedWordList( match.getProperty("words"));
				m.setLetters( match.getProperty("letters"));
				matches.add(m);
				
				//scores.add(Integer.parseInt(PixelWordClient.readData()));
			}
		}
		Properties getFollows = new Properties();
		getFollows.put("tag", "GET_FOLLOWS");
		getFollows.put("id", this.ID+"");
		PixelWordClient.sendObject(getFollows);
		
		Properties followsCount = (Properties) PixelWordClient.readObject();
		
		if(followsCount.getProperty("tag").equals("FOLLOWS_COUNT")){
			
			for( int i = 0 ; i < Integer.parseInt(followsCount.getProperty("size")) ; i++ ){
				
				Properties follow = (Properties) PixelWordClient.readObject();
				
				if(follow.getProperty("tag").equals("FOLLOW")){
					
					follows.add( Integer.parseInt(follow.getProperty("id") ) );
				}
			}
		}
	}
	public int getID(){
		return this.ID;
	}
	public String getAlias() {
		return this.alias;
	}
	
	public String getEmail() {
		return this.email;
	}
	
	public String getStatus(){
		if(this.status.equals("STATUS_NULL"))
			return "Did not specified a status yet";
		return this.status;
	}
	
	
	public void setStatus(String status){
		if(status.equals(""))
			this.status = "STATUS_NULL";
		else
			this.status = status;
	}
	
	public Match getHighScore(){
		int highScore = 0;
		int highScoreIndex = 0;
		for(int i = 0 ; i < matches.size() ; i++ )
			if( matches.get(i).getScore() > highScore  ){
				highScore = matches.get(i).getScore();
				highScoreIndex = i;
			}
		if(matches.size() != 0){
			return matches.get(highScoreIndex);
		}
		return new Match(this, 0, CurrentPlayer.preferredLanguage, 0);
	}
	
	public String toString(){
		String result;
		result = ""+this.ID;
		result += this.alias;
		result += this.follows;
		return result;
	}
	
	public boolean equals(Player temp){
		if(temp.getID() == this.ID){
			return true;
		}
		return false;
	}
	
	public boolean equals(int ID){
		if( ID == this.ID ){
			return true;
		}
		return false;
	}
	
	public String getLastPlayedTime(){
		Collections.sort(this.matches, new MyDateComparable());
		if(!this.matches.isEmpty())
			return matches.get(0).getDateTime();
		return "Never Played";
	}
	
	public ArrayList<Match> getLastMatchesFromFollows(){
		ArrayList<Match> allMatches = new ArrayList<Match>();
		for(int i = 0 ; i < this.follows.size() ; i++ ){
			Player tempPlayer = new Player(this.follows.get(i));
			allMatches.addAll(tempPlayer.matches);
		}
		Collections.sort(allMatches, new MyDateComparable());
		
		ArrayList<Match> lastScores = new ArrayList<Match>(10);
		
		int i = 0;
		while( i < allMatches.size() && i < 10){
			lastScores.add(allMatches.get(i));
			i++;
		}
		
		return lastScores;
	}
	
	public ArrayList<Match> getHighMatchesFromFollows(){
		ArrayList<Match> allMatches = new ArrayList<Match>();
		for(int i = 0 ; i < this.follows.size() ; i++ ){
			Player tempPlayer = new Player(this.follows.get(i));
			allMatches.addAll(tempPlayer.matches);
		}
		Collections.sort(allMatches, new MyScoreComparable());
		
		ArrayList<Match> highScores = new ArrayList<Match>(10);
		
		int i = 0;
		while( i < allMatches.size() && i < 10){
			highScores.add(allMatches.get(i));
			i++;
		}
		
		return highScores;
	}
	
	private class MyScoreComparable implements Comparator<Match>{
		public int compare(Match o1, Match o2) {
			if(o1.getScore() > o2.getScore())
				return -1;
			else if(o1.getScore() == o2.getScore())
				return 0;
			else
				return 1;
	    }
	}
	
	private class MyDateComparable implements Comparator<Match>{
		public int compare(Match o1, Match o2) {
			if(o1.getDateTime().compareTo(o2.getDateTime()) > 0)
				return -1;
			else if(o1.getDateTime().equals(o2.getDateTime()) )
				return 0;
			else
				return 1;
	    }
	}
	public boolean isFollowing(Player p){
		for(int i = 0 ; i < this.follows.size() ; i++ )
			if(this.follows.get(i) == p.getID() )
				return true;
		return false;
	}
	
	public boolean isFollowing(int ID){
		for(int i = 0 ; i < this.follows.size() ; i++ )
			if(this.follows.get(i) == ID )
				return true;
		return false;
	}
	
	public void updateFollows(){
		this.follows.clear();
		PixelWordClient.sendData("GET_FOLLOWS " + this.ID);
		
		String readData = PixelWordClient.readData();
		Scanner scan;
		int size;
		String followString;
		
		scan = new Scanner(readData);
		if(readData.startsWith("FOLLOWS_COUNT")){
			scan.next();
			size = scan.nextInt();
			for( int i = 0 ; i < size ; i++ ){
				followString = PixelWordClient.readData();
				scan = new Scanner(followString);
				
				if(followString.startsWith("FOLLOW")){
					scan.next();
					this.follows.add( scan.nextInt() );
				}
			}
		}
		follows.add( this.ID);
		scan.close();
	}
	
	public JPanel getMiniPanel(){
		JPanel miniPanel = new JPanel();
		JLabel aliasLabel = new JLabel(this.alias);
		JLabel scoreLabel = new JLabel(this.getHighScore().toString());
		JLabel statusLabel = new JLabel(this.getStatus());
		miniPanel.setLayout(new BoxLayout(miniPanel, BoxLayout.Y_AXIS));
		
		aliasLabel.setHorizontalAlignment(SwingConstants.LEFT);
		//aliasLabel.setFont(new Font("Calibri", Font.BOLD, 24));
		scoreLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		//scoreLabel.setFont(new Font("Calibri", Font.BOLD, 24));
		statusLabel.setHorizontalAlignment(SwingConstants.LEFT);
		statusLabel.setFont(CurrentPlayer.defFont.deriveFont(Font.ITALIC));
		
		miniPanel.setPreferredSize(new Dimension(200,100));

		miniPanel.add(aliasLabel);
		miniPanel.add(scoreLabel);
		miniPanel.add(statusLabel);
		
		if( this.getID() == CurrentPlayer.player.getID() ){
			miniPanel.setBackground(Color.red);
		}
		return miniPanel;
	}
	
	public JPanel getMiniPanelLinked(MouseListener listener){
		JPanel miniPanel = new JPanel();
		JLabel aliasLabel = new JLabel(this.alias);
		JLabel scoreLabel = new JLabel(this.getHighScore().toString());
		JLabel statusLabel = new JLabel(this.getStatus());
		miniPanel.setLayout(new BoxLayout(miniPanel, BoxLayout.Y_AXIS));
		
		//aliasLabel.setFont(new Font("Calibri", Font.BOLD, 24));
		aliasLabel.addMouseListener(listener);
		//scoreLabel.setFont(new Font("Calibri", Font.BOLD, 24));
		statusLabel.setHorizontalAlignment(SwingConstants.LEFT);
		statusLabel.setFont(CurrentPlayer.defFont.deriveFont(Font.ITALIC));

		miniPanel.setPreferredSize(new Dimension(200,100));
		miniPanel.setMaximumSize(new Dimension(200,100));
		miniPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));

		miniPanel.add(aliasLabel);
		miniPanel.add(scoreLabel);
		miniPanel.add(statusLabel);
		
		return miniPanel;
	}

}
