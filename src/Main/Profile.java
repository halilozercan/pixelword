/*
 * Author: Safa Onur Sahin
 * 
 */
package Main;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

import Connection.PixelWordClient;

import java.awt.*;
import java.awt.event.*;
import java.util.Properties;
import java.util.Scanner;

public class Profile extends JPanel implements Updatable{
	private Environment environmentPanel;
	private JPanel p1, findPanel;
	private JLabel currentLanguage;
	private CurrentProfile currentProfile;
	private JButton playButton, findPlayer;
	private JTextField playerEmail;
	private GameCaller callGame;
	private Player p;
	
	public Profile(Player p){
	
		this.p = p;
		
		setPreferredSize(new Dimension(800,600));
		
		setLayout(new BorderLayout());
		
		Listener listener = new Listener();
		
		currentProfile = new CurrentProfile(p, this);
		
		p1 = new JPanel();
		p1.setPreferredSize(new Dimension(325,200));
		p1.setOpaque(false);
		p1.setLayout(new BoxLayout(p1, BoxLayout.Y_AXIS));
		
		playButton = new JButton("Play!");
		playButton.addActionListener(listener);
		playButton.setPreferredSize(new Dimension(160,40));
		playButton.setMaximumSize(new Dimension(160,40));
		
		currentLanguage = new JLabel("Language: " + CurrentPlayer.preferredLanguage.substring(0,1).toUpperCase() + CurrentPlayer.preferredLanguage.substring(1));
		//currentLanguage.setFont( new Font("Calibri", Font.LAYOUT_LEFT_TO_RIGHT, 20) );
		
		findPlayer = new JButton("Find!");
		findPlayer.addActionListener(listener);
		
		playerEmail = new JTextField(32);
		playerEmail.setPreferredSize(new Dimension(200,30));
		playerEmail.setMaximumSize(new Dimension(200,30));
		//playerEmail.setFont(new Font("Calibri", Font.PLAIN, 20));
		
		
		findPanel = new JPanel();
		findPanel.setLayout(new BoxLayout(findPanel, BoxLayout.X_AXIS));
		findPanel.add(playerEmail);
		findPanel.add(Box.createRigidArea(new Dimension(10,0)));
		findPanel.add(findPlayer);
		
		p1.setBorder(new EmptyBorder(0,5,5,5));
		p1.add(Box.createRigidArea(new Dimension(0, 20)));
		p1.add(playButton);
		p1.add(Box.createRigidArea(new Dimension(0, 10)));
		p1.add(currentLanguage);
		p1.add(Box.createRigidArea(new Dimension(0, 70)));
		p1.add(findPanel);
		
		for (Component c : p1.getComponents()) {
		    if(c instanceof JComponent) {
		        ((JComponent)c).setAlignmentX(Component.RIGHT_ALIGNMENT);
		    }
		}
		
		environmentPanel = new Environment(CurrentPlayer.player, this);
		environmentPanel.setPreferredSize(new Dimension(750,400));
		
		
		add(currentProfile, BorderLayout.WEST);
		add(p1, BorderLayout.EAST);
		add(environmentPanel, BorderLayout.PAGE_END);
	}
	public void paintComponent(Graphics g){
		g.drawImage( new ImageIcon("bg.jpg").getImage(),0,0,null);
	}
	
	public CurrentProfile getCurrentProfile(){
		return currentProfile;
	}

	private class Listener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			if(event.getSource() == playButton){
				callGame = new GameCaller( CurrentPlayer.preferredLanguage, currentProfile.parentProfile );
			}
			
			if(event.getSource() == findPlayer)
			{
				Properties isPlayer = new Properties();
				isPlayer.put("tag", "IS_PLAYER");
				isPlayer.put("email", playerEmail.getText());
				PixelWordClient.sendObject(isPlayer);
				
				Properties isPlayerAnswer = (Properties) PixelWordClient.readObject();
				if(isPlayerAnswer.getProperty("tag").equals("PLAYER_DETAILS")){
					
					int ID = Integer.parseInt(isPlayerAnswer.getProperty("id"));
					currentProfile.update(new Player(ID));
					
				}
					
				else if(isPlayerAnswer.getProperty("tag").equals("NO_ACCOUNT")){
					JOptionPane.showMessageDialog(null, "Account does not exist");
				}
				
			}
		}
	}

	@Override
	public void update(Player p) {

		environmentPanel.update(p);
		currentProfile.update(p);
		
	}
}
