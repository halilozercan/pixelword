/*
 * Author: Safa Onur Sahin
 * 
 */
package Main;
import javax.swing.*;
import javax.swing.border.TitledBorder;

import Connection.PixelWordClient;

import java.awt.*;
import java.awt.event.*;

public class CurrentProfile extends JTabbedPane implements Updatable{
	
	private JPanel profileDetails, profileTab, matches, followPanel;
	private JLabel l1, l2, l3, l4, l5;
	private JButton followButton, turnBackToProfile, editProfileButton;
	private Player p;
	public  Profile parentProfile;
	
	public CurrentProfile(Player p, Profile parentProfile){
		
		this.p = p;
		this.parentProfile = parentProfile;
		setPreferredSize(new Dimension(500,200));
		//TitledBorder leftBorder = BorderFactory.createTitledBorder( p.getAlias() );
	    //leftBorder.setTitleJustification(TitledBorder.LEFT);
	    //setBorder(leftBorder);
		initComponents();
		
	}
	
	public void initComponents(){
		profileTab = new JPanel();
		profileDetails = new JPanel();
		profileDetails.setPreferredSize(new Dimension(300, 300));
		l1 = new JLabel("Highest Score : " + p.getHighScore());
		//l1.setFont(new Font("Calibri",Font.BOLD, 15));
		l2 = new JLabel(p.getEmail());
		//l2.setFont(new Font("Calibri",Font.BOLD, 15));
		l4 = new JLabel("Last Played : " + p.getLastPlayedTime());
		//l4.setFont(new Font("Calibri",Font.BOLD, 15));
		l3 = new JLabel(p.getStatus());
		//l3.setFont(new Font("Calibri",Font.BOLD, 15));
		
		profileDetails.setLayout(new BoxLayout(profileDetails, BoxLayout.Y_AXIS));
		profileDetails.add(Box.createRigidArea(new Dimension(0, 15)));
		profileDetails.add(l1);
		profileDetails.add(Box.createRigidArea(new Dimension(0, 15)));
		profileDetails.add(l2);
		profileDetails.add(Box.createRigidArea(new Dimension(0, 15)));
		profileDetails.add(l4);
		profileDetails.add(Box.createRigidArea(new Dimension(0, 15)));
		profileDetails.add(l3);
		
		followPanel = new JPanel();
		followPanel.setPreferredSize(new Dimension(150,300));
		followPanel.setLayout(new BoxLayout(followPanel, BoxLayout.Y_AXIS));
		if(!CurrentPlayer.player.equals(p)){
			followButton = new JButton("");
			if(CurrentPlayer.player.isFollowing(p))
				followButton.setText("Unfollow");
			else
				followButton.setText("Follow");
			followButton.addActionListener(new FollowListener());
			followPanel.add(followButton);
			
			turnBackToProfile = new JButton("Go to Profile..");
			turnBackToProfile.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e){
					update(CurrentPlayer.player);
				}
			});
			followPanel.add(turnBackToProfile);
		}
		else{
			editProfileButton = new JButton("Edit Profile");
			editProfileButton.addActionListener(new EditListener(this));
			editProfileButton.setAlignmentY(Component.BOTTOM_ALIGNMENT);
			followPanel.add(editProfileButton);
		}
		profileTab.add(profileDetails);
		profileTab.add(followPanel);
		
		matches = new JPanel();
		matches.setLayout(new BoxLayout(matches, BoxLayout.Y_AXIS));
		matches.setOpaque(false);
		
		for(int i = 0 ; i < p.matches.size() ; i++ ){
			JPanel scorePanel = new JPanel();
			scorePanel.setPreferredSize(new Dimension(425,40));
			scorePanel.addMouseListener(new MatchListener(p.matches.get(i)));
			scorePanel.setOpaque(false);
			
			JLabel dateLabel = new JLabel(""+p.matches.get(i).getDateTime());
			//dateLabel.setFont(new Font("Calibri", Font.BOLD, 15));
			dateLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
			dateLabel.setForeground(Color.gray);
			//datePanel.setBorder(BorderFactory.createRaisedBevelBorder());
			
			
			JLabel scoreLabel = new JLabel(""+p.matches.get(i).getScore());
			//scoreLabel.setFont(new Font("Calibri", Font.BOLD, 15));
			scoreLabel.setForeground(Color.red);
			scoreLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
			//scorePanel.setBorder(BorderFactory.createLoweredBevelBorder());
			
			
			JLabel lettersLabel = new JLabel(""+p.matches.get(i).getLetters());
			//scoreLabel.setFont(new Font("Calibri", Font.BOLD, 15));
			lettersLabel.setForeground(Color.red);
			lettersLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
			//scorePanel.setBorder(BorderFactory.createLoweredBevelBorder());
			
			scorePanel.add(dateLabel);
			scorePanel.add(scoreLabel);
			scorePanel.add(lettersLabel);
			
			matches.add(scorePanel);
			
		}
		
		JScrollPane scrollableScores = new JScrollPane(matches);
		this.addTab(p.getAlias(), profileTab);
		
		this.addTab("Games", scrollableScores);
	}
	
	private class FollowListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			if(((JButton)e.getSource()).getText().equals("Unfollow")){
				PixelWordClient.sendData("UN_FOLLOW " + CurrentPlayer.player.getID() + " " + p.getID());
				String readData = PixelWordClient.readData();
				if(readData.equals("SUCCESS")){
					JOptionPane.showMessageDialog((Component) e.getSource(), "Successfully Unfollewed");
					((JButton)e.getSource()).setText("Follow");
					
				}
			}
			else if(((JButton)e.getSource()).getText().equals("Follow")){
				PixelWordClient.sendData("ADD_FOLLOW " + CurrentPlayer.player.getEmail() + " " + p.getEmail());
				String readData = PixelWordClient.readData();
				if(readData.equals("ADDED_FOLLOW")){
					JOptionPane.showMessageDialog((Component) e.getSource(), "Successfully Follewed");
					((JButton)e.getSource()).setText("Unfollow");
				}
			}
			CurrentPlayer.player.updateFollows();
			parentProfile.update(CurrentPlayer.player);
		}
	}
	
	private class EditListener implements ActionListener{
		CurrentProfile parent;
		public EditListener(CurrentProfile parent){
			this.parent = parent;
		}
		public void actionPerformed(ActionEvent e){
			EditProfile editProfileFrame = new EditProfile(p, parent); 
		}
	}
	
	
	private class MatchListener extends MouseAdapter{
		Match m;
		public MatchListener(Match m){
			this.m = m;
		}
		public void mouseClicked(MouseEvent e){
			m.getFrameOfMatch();
		
		}
		
		public void mouseEntered(MouseEvent e) {
		     Cursor cursor = Cursor.getDefaultCursor();
		     cursor = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR); 
		     setCursor(cursor);
		  }
		public void mouseExited(MouseEvent e) {
		     Cursor cursor = Cursor.getDefaultCursor();
		     cursor = Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR); 
		     setCursor(cursor);
		  }
	}

	@Override
	public void update(Player p) {
		this.p = p;
		this.removeAll();
		initComponents();
		
	}
}
