/*
 * Author: Kaan Aky�z
 * 
 */
package GamePlay;
import java.awt.*;

import javax.swing.*;


public class LettersPanel extends JPanel{
	
	private JLabel label1;
	private String word;
	private JPanel container;
	
	public LettersPanel(String word){
		
		this.word = word;
		this.setOpaque(false);
		setPreferredSize(new Dimension(600, 50));
		label1 = new JLabel("Letter found : ");
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		add(label1, BorderLayout.WEST);
		container = new JPanel();
		container.setOpaque(false);
		container.setAlignmentX(LEFT_ALIGNMENT);
		add(container, BorderLayout.CENTER);
		
	}
	
	public void updateList(int bound){
		JPanel panel = new JPanel();
		panel.setPreferredSize(new Dimension(24,24));
		panel.setOpaque(false);
		JLabel label = new JLabel(""+word.charAt(bound-1) );
		//label.setFont(new Font("Calibri", Font.BOLD, 20));
		panel.add( label );
		container.add(panel);
	
		
		
	}

}
