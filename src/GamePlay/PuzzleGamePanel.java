package GamePlay;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import PixelPuzzle.GameModel;
import Main.*;

class PuzzleGamePanel extends JPanel{
		private GameModel puzzleGame;
		public JButton passToAnagram;
		public PuzzleGamePanel(StringBuffer letters, final Game hugeGame){
			setPreferredSize(new Dimension(800,600));
			setOpaque(false);
			
			java.awt.Canvas canvas = new java.awt.Canvas();
			canvas.setSize(800,550);
			canvas.setBackground(Color.white);
			
			passToAnagram = new JButton("Pass!");
			passToAnagram.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e){
					hugeGame.anagramGamePanel.getAnagramGame().setLetters(hugeGame.puzzleGamePanel.getPuzzleGame().getFoundSoFar());
					hugeGame.anagramGamePanel.update();
					
					hugeGame.puzzleGamePanel.getPuzzleGame().destroyDisplay();

					hugeGame.allLettersFound();
				}
			});
			passToAnagram.setEnabled(false);
			
			add(canvas);
			
			add(passToAnagram);
			
			puzzleGame = new GameModel(letters.toString(), canvas, hugeGame);
		}
		
		public GameModel getPuzzleGame(){
			return puzzleGame;
		}
	}