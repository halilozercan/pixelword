package GamePlay;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import WordGame.GameAnagram;

public class GameOverPanel extends JPanel{
		private JLabel gameOver;
		private JLabel welcome;
		
		public GameOverPanel(GameAnagram game){
			System.out.println("here gameoverpanel gameanagram");
			this.setOpaque(false);
			this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
			gameOver = new JLabel("Game is over! Your Score: " + game.score);
			gameOver.setFont(new Font("Calibri", Font.PLAIN, 24));
			add(gameOver);
			JPanel words = new JPanel();
			words.setLayout(new GridLayout(0,8));
			ArrayList<String> availableWords = game.getAvailableWords();
			for(int i = 0 ;  i < availableWords.size(); i++ ){
				JLabel temp = new JLabel(availableWords.get(i));
				System.out.println("here gameoverpanel gameanagram");
				temp.addMouseListener(new OpenURLListener("http://www.seslisozluk.net/?word=" + availableWords.get(i)));
				System.out.println("here gameoverpanel gameanagram");
				temp.setFont(new Font("Calibri", Font.PLAIN, 15));
				words.add(temp);
			}
			JScrollPane scrollableWords = new JScrollPane(words);
			add(scrollableWords);
		}
		public GameOverPanel(boolean neverPlayed){
			this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
			welcome = new JLabel("Welcome to PixelWord again. Press 'New Game' to have some fun if you want");
			welcome.setFont(new Font("Calibri", Font.PLAIN, 24));
			add(welcome);
		}
		
		private class OpenURLListener extends MouseAdapter{
			String address;
			public OpenURLListener(String address){
				this.address = address;
			}
			public void openURL(String address) throws URISyntaxException{
				URI uri = new URI(address);
				if (Desktop.isDesktopSupported()) {
				      try {
				        Desktop.getDesktop().browse(uri);
				      } catch (IOException e) { }
				    } else {  }
			}
			public void mouseClicked(MouseEvent e){
				try {
					openURL(address);
				} catch (URISyntaxException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			
			}
			
			public void mouseEntered(MouseEvent e) {
			     Cursor cursor = Cursor.getDefaultCursor();
			     cursor = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR); 
			     setCursor(cursor);
			  }
			public void mouseExited(MouseEvent e) {
			     Cursor cursor = Cursor.getDefaultCursor();
			     cursor = Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR); 
			     setCursor(cursor);
			  }
		}
	}