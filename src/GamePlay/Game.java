/*
 * Author: Kaan Aky�z
 * 
 */
package GamePlay;
import javax.imageio.ImageIO;
import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import Connection.PixelWordClient;
import Main.*;
import WordGame.*;

import javax.swing.border.EmptyBorder;

public class Game extends JPanel{
	
	private static final int TIME_STEP = 100;
	private static final int FINAL_TIME = 180000;
	
	private JPanel containerPanel, statPanel, scrabbleGamePanel, gameOverPanel, scoresPanel;
	public  PuzzleGamePanel puzzleGamePanel;
	public  AnagramGamePanel anagramGamePanel;
	private JTabbedPane gameScreen;
	private JLabel l1, l2;
	private String time;
	private Timer timer;
	public  int timeCount;
	private StringBuffer letters;
	private Profile parentProfile;

	private JFrame mainFrame;
	private String language;
	
	public Game(String language, JFrame frame, Profile parentProfile){
		
		this.mainFrame = frame;
		this.language = language;
		this.parentProfile = parentProfile;
		
		
		
		newGame();
		
		
		//StatPanel
		statPanel = new JPanel();
		statPanel.setPreferredSize(new Dimension(300, 600));
		statPanel.setOpaque(false);
		l1 = new JLabel("Language :  " + this.language.substring(0,1).toUpperCase() + this.language.substring(1));
		//l1.setFont(new Font("Calibri", Font.LAYOUT_LEFT_TO_RIGHT, 15));
		l2 = new JLabel("Time left : " + this.time);
		l2.setFont(new Font("Calibri", Font.LAYOUT_LEFT_TO_RIGHT, 20));
		
		scoresPanel = new JPanel();
		scoresPanel.setLayout(new BoxLayout(scoresPanel, BoxLayout.Y_AXIS));
		scoresPanel.setBorder(new EmptyBorder(10,10,10,10));
		scoresPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		for(int i = 0 ; i < CurrentPlayer.player.getHighMatchesFromFollows().size() ; i++ ){
			scoresPanel.add(CurrentPlayer.player.getHighMatchesFromFollows().get(i).getMiniPanel());
			scoresPanel.add(Box.createRigidArea(new Dimension(0,10)));
		}
		
		JScrollPane scrollableScoresPanel = new JScrollPane(scoresPanel);
		statPanel.setLayout(new BoxLayout(statPanel, BoxLayout.PAGE_AXIS));
		statPanel.add(l1);
		statPanel.add(l2);
		statPanel.add(scrollableScoresPanel);
		
		
		drawGUI();
		
		timer = new Timer(TIME_STEP, new TimeListener());
		
		
	}
	
	public void newGame(){
		
		letters = new StringBuffer();
		letters = LanguageMethods.getRandomLetters( 10 , language , 4 , 4);
			
		timeCount = 0;
		
		
		time = stringTimeCalculator(timeCount);
		
		gameScreen = new JTabbedPane();
		

		try{
			gameOverPanel = new GameOverPanel(anagramGamePanel.getAnagramGame());
		}catch(Exception e){
			gameOverPanel = new GameOverPanel(false);
		}
		
		puzzleGamePanel = new PuzzleGamePanel(letters, this);
		anagramGamePanel = new AnagramGamePanel(letters, language, this);
		
		
		gameScreen.addTab("PuzzleGame", puzzleGamePanel);
		gameScreen.addTab("AnagramGame", anagramGamePanel);
		gameScreen.addTab("Outside the Box", gameOverPanel);
		
		
		gameScreen.setEnabledAt(0, false);
		gameScreen.setEnabledAt(1, false);
		gameScreen.setEnabledAt(2, false);
		
		
		gameScreen.setSelectedIndex(2);
		gameOverPanel.add(new GameStarter());
		
		containerPanel = new LettersPanel( letters.toString() );
	}

	
	public void drawGUI(){
		removeAll();
		setBorder(new EmptyBorder(10, 10, 10, 10) );
		BorderLayout newLayout = new BorderLayout(5,5);
		newLayout.setHgap(5);
		setLayout( newLayout );
		
		//adds
		add(gameScreen, BorderLayout.WEST);
		add(containerPanel, BorderLayout.PAGE_END);
		add(statPanel, BorderLayout.EAST);
	}
	
	public void paintComponent(Graphics g){
		try {
			g.drawImage( ImageIO.read(new File("bg.jpg")), 0, 0, null);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void gameOver(){
		
		if(anagramGamePanel.getAnagramGame().score != 0){
			new ScoreSender().start();
			CurrentPlayer.player = new Player(CurrentPlayer.player.getID());
			this.parentProfile.update(CurrentPlayer.player);
		}
		
		timer.stop();
		
		if(puzzleGamePanel.getPuzzleGame().isRunning())
			puzzleGamePanel.getPuzzleGame().destroyDisplay();
		
		newGame();
		
		drawGUI();
		
		
	}
	
	private class ScoreSender extends Thread{
		public void run(){
			String words = "";
			Properties match = new Properties();
			match.put("tag", "MATCH_SENT");
			match.put("userid", CurrentPlayer.player.getID()+"");
			match.put("score", anagramGamePanel.getAnagramGame().score+"");
			match.put("language", CurrentPlayer.preferredLanguage);
			
			for(int i = 0 ; i < anagramGamePanel.getAnagramGame().guessedWordsID.size() ; i++ ){
				words += anagramGamePanel.getAnagramGame().guessedWordsID.get(i) + " ";
			}
			
			match.put("words", words);
			match.put("letters", anagramGamePanel.getAnagramGame().getLetters());
			
			PixelWordClient.sendObject(match);
			
		}
	}
	
	private class TimeListener implements ActionListener{
		
		private boolean wordGameStarted;
		public void actionPerformed(ActionEvent event){
			
			timeCount += TIME_STEP;
			time = stringTimeCalculator(FINAL_TIME - timeCount); 
			l2.setText("Time Left : " + time);
			
			if(timeCount == FINAL_TIME){
				gameOver();
				
				
			}
		}
	}
	
	public static String stringTimeCalculator(int count)
	{
		String result = "";
		int miliSeconds = ( count % 1000 ) / 100;
		int seconds = ( count - ( count % 1000 ) ) / 1000;
		int secondsToWrite = seconds % 60;
		int minutes = (seconds - secondsToWrite) / 60;
		
		if( minutes < 10 )
			result += "0" + minutes +":";
		else
			result += minutes + ":";
		
		if( secondsToWrite < 10 )
			result += "0" + secondsToWrite +":";
		else
			result += secondsToWrite + ":";
		
		result += miliSeconds;
		
		return result;
	}
	
	private class GameStarter extends JButton{
		
		public GameStarter(){
			setText("New Game");
			addActionListener(new GameStarterListener());
		}
		
		private class GameStarterListener implements ActionListener{
			
			public void actionPerformed(ActionEvent e){
				

				gameScreen.setSelectedIndex(0);	
				puzzleGamePanel.getPuzzleGame().start();
				timer.start();		
			}
		}
	}
		
	
	
	public void allLettersFound(){
		gameScreen.setSelectedIndex(1);
		((LettersPanel) containerPanel).setVisible(false);
	}
	
	public void update(){
		int oldLetterTurn = 0;
		((LettersPanel) containerPanel).updateList( puzzleGamePanel.getPuzzleGame().getLetterTurn() );
		oldLetterTurn = puzzleGamePanel.getPuzzleGame().getLetterTurn();
		
		if(puzzleGamePanel.getPuzzleGame().getLetterTurn() > 3)
			puzzleGamePanel.passToAnagram.setEnabled(true);
	}
	
	
	public PuzzleGamePanel getPuzzleGamePanel(){
		return puzzleGamePanel;
	}
	
	public AnagramGamePanel getAnagramGamePanel(){
		return anagramGamePanel;
	}

}
